import Earthdawn4eActorSheet from './earthdawn-4e-actor-sheet.js';
import EarthdawnDialog from '../entities/earthdawn-dialog.js';
import { lpTrackingTableApp } from '../helpers/lp-spending.js';
import { rollPrep } from '../helpers/roll-prep.js';

export default class earthdawn4ePCSheet extends Earthdawn4eActorSheet {
  static get defaultOptions() {
    return mergeObject(super.defaultOptions, {
      width: 800,
      height: 850,
      classes: ['earthdawn', 'sheet', 'actor', 'characteristicsTab', 'storyNotesTab'],
      tabs: [
        {
          navSelector: '.sheet-tabs',
          contentSelector: '.sheet-body',
          initial: 'main',
        },
      ],
    });
  }

  get template() {
    return `systems/earthdawn4e/templates/actors/sheets/pc-sheet.hbs`;
  }

  //HTML enrich
  async _enableHTMLEnrichment() {
    let enrichment = {};
    enrichment["system.description"] = await TextEditor.enrichHTML(this.actor.system.description, {async: true, secrets: this.actor.isOwner});
    enrichment["system.biography"] = await TextEditor.enrichHTML(this.actor.system.biography, {async: true, secrets: this.actor.isOwner});
    return expandObject(enrichment);
  }

  async getData() {
    const sheetData = super.getData();
    //const itemData = data.data;
    //data.item = itemData;
    //data.data = itemData.system;

    //data.type = this.actor.system.type;
    sheetData.weapons = sheetData.items.filter((item) => item.type === 'weapon');
    sheetData.talents = sheetData.items.filter((item) => item.type === 'talent');
    sheetData.skills = sheetData.items.filter((item) => item.type === 'skill');
    sheetData.devotions = sheetData.items.filter((item) => item.type === 'devotion');
    sheetData.armor = sheetData.items.filter((item) => item.type === 'armor');
    sheetData.equipment = sheetData.items.filter((item) => item.type === 'equipment');
    sheetData.spells = sheetData.items.filter((item) => item.type === 'spell');
    sheetData.disciplines = sheetData.items.filter((item) => item.type === 'discipline');
    sheetData.namegivers = sheetData.items.filter((item) => item.type === 'namegiver');
    sheetData.matrixes = sheetData.items.filter((item) => item.type === 'spellmatrix');
    sheetData.shields = sheetData.items.filter((item) => item.type === 'shield');
    sheetData.attacks = sheetData.items.filter((item) => item.type === 'attack');
    sheetData.threads = sheetData.items.filter((item) => item.type === 'thread');
    sheetData.knacks = sheetData.items.filter((item) => item.type === 'knack');
    sheetData.equippedweapons = sheetData.items.filter((item) => item.type === 'weapon' && item.system.worn === true);
    sheetData.favorites = sheetData.items.filter((item) => item.type === 'talent' && item.system.favorite === 'true');

    sheetData.namegiver = {};
    if (sheetData.namegivers.length > 0) {
      sheetData.namegiver.dexterity = sheetData.namegivers[0].system.attributes.dexterityvalue;
      sheetData.namegiver.strength = sheetData.namegivers[0].system.attributes.strengthvalue;
      sheetData.namegiver.toughness = sheetData.namegivers[0].system.attributes.toughnessvalue;
      sheetData.namegiver.perception = sheetData.namegivers[0].system.attributes.perceptionvalue;
      sheetData.namegiver.willpower = sheetData.namegivers[0].system.attributes.willpowervalue;
      sheetData.namegiver.charisma = sheetData.namegivers[0].system.attributes.charismavalue;
    }

    this._setChargenAttributes(sheetData);

    sheetData.enrichment =  await this._enableHTMLEnrichment();

    console.log('[EARTHDAWN] PC Data', sheetData);

    return sheetData;
  }

  activateListeners(html) {
    super.activateListeners(html);
    this.baseListeners(html);

    // Drag event handler
    const dragHandler = (ev) => this._onDragStart(ev);

    // Helper function to make things draggable
    const makeDraggable = function (index, element) {
      // Add draggable attribute and dragstart listener.
      element.setAttribute('draggable', true);
      element.addEventListener('dragstart', dragHandler, false);
    };

    html.find('.item-create').click(this._onItemCreate.bind(this));

    html.find('.item-draggable').each(makeDraggable);

    html.find('.recovery-roll').click(() => {
      this.actor.recoveryTest();
    });

    html.find('.new-day').click(() => {
      this.actor.newDay();
    });

    html.find('.attribute-roll').click((ev) => {
      const att = $(ev.currentTarget).attr('data-att');
      const name = $(ev.currentTarget).attr('data-name');
      this._attributeRoll(att, name);
    });

    html.find('item-test').click(() => {
      this.actor.getspells();
    });

    // html.find('.att-change-button').click((ev) => {
    //   this._attributeChange(ev);
    // });

    html.find('.finalize').click(async () => {
      await this._finalizeBuild();
    });

    html.find('.link-checkbox').click(async (ev) => {
      ev.preventDefault();
      const li = $(ev.currentTarget).parents('.item-name');
      const item = this.actor.items.get(li.data('itemId'));

      if (item.type === 'armor' || item.type === 'shield' || item.type === 'weapon' || item.type === 'equipment') {
        await item.update({ 'system.worn': ev.target.checked });
      } else if (item.type === 'thread') {
        await item.update({ 'system.active': ev.target.checked });
      }
    });

    html.find('.talent-roll').click((ev) => {
      const li = $(ev.currentTarget).parents('.item-name');
      // let itemID = li.data('itemId');
      // V10 changes
      let itemID = li.data('itemId');
      // change End
      this._talentRoll(itemID);
    });

    html.find('.attack-roll').click((ev) => {
      const li = $(ev.currentTarget).parents('.item-name');
      // let itemID = li.data('itemId');
      // V10 changes
      let itemID = li.data('itemId');
      // change End
      this._attackRoll(itemID);
    });

    html.find('.attuneMatrix').click((ev) => {
      const li = $(ev.currentTarget).parents('.item-name');
      // let itemID = li.data('itemId');
      // V10 changes
      let itemID = li.data('itemId');
      // change End
      this._attuneMatrix(itemID);
    });

    html.find('.weaveThread').click((ev) => {
      const li = $(ev.currentTarget).parents('.item-name');
      // let itemID = li.data('itemId');
      // V10 changes
      let itemID = li.data('itemId');
      // change End
      this._weaveThread(itemID);
    });

    html.find('.castSpell').click((ev) => {
      const li = $(ev.currentTarget).parents('.item-name');
      // let itemID = li.data('itemId');
      // V10 changes
      let itemID = li.data('itemId');
      // change End
      this._castSpell(itemID);
    });

    html.find('.clearMatrix').click((ev) => {
      const li = $(ev.currentTarget).parents('.item-name');
      // const matrix = this.actor.items.get(li.data('itemId'));
      // V10 changes
      const matrix = this.actor.items.get(li.data('itemId'));
      // change End
      this.actor.clearMatrix(matrix);
    });

    html.find('.effectTest').click((ev) => {
      const li = $(ev.currentTarget).parents('.item-name');
      // const weapon = this.actor.items.get(li.data('itemId'));
      // V10 changes
      const weapon = this.actor.items.get(li.data('itemId'));
      // change End
      this.actor.weaponDamagePrep(weapon, 0);
    });

    html.find('.half-magic').click(() => {
      this.actor.halfMagic();
    });

    html.find('.knack-roll').click((ev) => {
      const li = $(ev.currentTarget).parents('.item-name');
      // let itemID = li.data('itemId');
      // V10 changes
      let itemID = li.data('itemId');
      // change End
      let item = this.actor.items.get(itemID);
      // let parentID = item.data.data.sourceTalentId;
      // V10 changes
      let parentID = item.system.sourceTalentId;
      // change End
      if (parentID === null) {
        ui.notifications.error(game.i18n.localize('earthdawn.k.knackNoParent'));
      }
      // let attribute = item.data.data.attribute;
      // V10 changes
      let attribute = item.system.attribute;
      // change End

      this._knackRoll(parentID, item);
    });

    html.find('.showLPtracking').click(async () => {
      let d = new lpTrackingTableApp(this.actor);
      d.render(true);
    });

    html.find('.chargen').click(async () => {
      const CharGenHBS = await renderTemplate(
        'systems/earthdawn4e/templates/taka-theme/actors/partials/dialog/chargen-dialog.hbs',
        await this.getData(),
      );
      console.log("looking at SheetData "+this.getData());

      new EarthdawnDialog({
        title: game.i18n.localize('earthdawn.c.characterGeneration'),
        content: CharGenHBS,
        buttons: {
          finalize: {
            label: game.i18n.localize('earthdawn.f.finalizeBuild'),
            callback: () => {
              this._finalizeBuild();
            },
          },
          cancel: {
            label: game.i18n.localize('earthdawn.c.cancel'),
          },
        },
        default: 'ok',
      }).render(true);
    });

    html.find('.override').click(async () => {
      const OverrideHBS = await renderTemplate(
        'systems/earthdawn4e/templates/taka-theme/actors/partials/dialog/override-dialog.hbs',
        this.getData(),
      );

      new EarthdawnDialog({
        title: game.i18n.localize('earthdawn.o.override'),
        content: OverrideHBS,
        buttons: {
          finalize: {
            label: game.i18n.localize('earthdawn.o.ok'),
            callback: () => {
              this._finalizeOverride();
            },
          },
          cancel: {
            label: game.i18n.localize('earthdawn.c.cancel'),
          },
        },
        default: 'ok',
      }).render(true);
    });
  }

  _attributeRoll(att, name) {
    let inputs = { attribute: att, name: name };
    this.actor.rollPrep(inputs);
  }

  _talentRoll(talentID) {
    let inputs = { talentID: talentID };
    rollPrep(this.actor, inputs);
  }

  _attackRoll(weaponID) {
    let inputs = { weaponID: weaponID, rolltype: 'attack' };
    this.actor.rollPrep(inputs);
  }

  _attuneMatrix(matrixID) {
    const matrix = this.actor.items.get(matrixID);
    this.actor.attuneMatrix(matrix);
  }

  _weaveThread(matrixID) {
    const matrix = this.actor.items.get(matrixID);
    this.actor.weaveThread(matrix);
  }

  _knackRoll(talentID, knack) {
    let talent = this.actor.items.get(talentID);
    let inputs = {
      // ranks: talent.data.data.ranks,
      // talentID: talentID,
      // attribute: knack.data.data.attribute,
      // strain: knack.data.data.strain,
      // V10 changes
      ranks: talent.system.ranks,
      talentID: talentID,
      attribute: knack.system.attribute,
      strain: knack.system.strain,
      // change End
      talentName: knack.name,
    };

    this.actor.rollPrep(inputs);
  }

  _castSpell(matrixID) {
    const matrix = this.actor.items.get(matrixID);
    this.actor.castSpell(matrix);
  }

  _onItemCreate(event) {
    event.preventDefault();
    let element = event.currentTarget;
    let itemData = {
      name: game.i18n.localize('earthdawn.n.newItem'),
      type: element.dataset.type,
    };

    return this.actor.createEmbeddedDocuments('Item', [itemData]);
  }


  // let data1 = getData();
  // console.log('[EARTHDAWN] data 1 ' + data1);

  async _finalizeBuild() {
    console.log('[EARTHDAWN] Finalize Build');
    const sheetData = await this.getData();
    console.log("sheetData: " + sheetData);

    if (Number(document.getElementsByName('system.totalremaining')[0].value) >= 0) {
      let newdexterity = Number(document.getElementsByName('system.dexteritytotal')[0].value);
      let newstrength = Number(document.getElementsByName('system.strengthtotal')[0].value);
      let newtoughness = Number(document.getElementsByName('system.toughnesstotal')[0].value);
      let newperception = Number(document.getElementsByName('system.perceptiontotal')[0].value);
      let newwillpower = Number(document.getElementsByName('system.willpowertotal')[0].value);
      let newcharisma = Number(document.getElementsByName('system.charismatotal')[0].value);
      let unspentpoints = Number(document.getElementsByName('system.totalremaining')[0].value);
      
      if (newdexterity == 0) {
        newdexterity = sheetData.namegiver.dexterity
      }
      if (newstrength == 0) {
        newstrength = sheetData.namegiver.strength
      }
      if (newtoughness == 0) {
        newtoughness = sheetData.namegiver.toughness
      }
      if (newperception == 0) {
        newperception = sheetData.namegiver.perception
      }
      if (newwillpower == 0) {
        newwillpower = sheetData.namegiver.willpower
      }
      if (newcharisma == 0 ) {
        newcharisma = sheetData.namegiver.charisma
      }


      await this.actor.update({
        'system.attributes.dexterityinitial': newdexterity,
        'system.attributes.strengthinitial': newstrength,
        'system.attributes.toughnessinitial': newtoughness,
        'system.attributes.perceptioninitial': newperception,
        'system.attributes.willpowerinitial': newwillpower,
        'system.attributes.charismainitial': newcharisma,
        'system.attributes.dexterityvalue': newdexterity,
        'system.attributes.strengthvalue': newstrength,
        'system.attributes.toughnessvalue': newtoughness,
        'system.attributes.perceptionvalue': newperception,
        'system.attributes.willpowervalue': newwillpower,
        'system.attributes.charismavalue': newcharisma,
        'system.unspentattributepoints': unspentpoints,
      });
    } else {
      ui.notifications.error(game.i18n.localize('earthdawn.c.charGen2ManyPoints'));
    }
  }

  async _finalizeOverride() {
    console.log('[EARTHDAWN] Finalize Override');
    let physicaldefense = Number(document.getElementsByName('system.overrides.physicaldefense')[0].value);
    let mysticdefense = Number(document.getElementsByName('system.overrides.mysticdefense')[0].value);
    let socialdefense = Number(document.getElementsByName('system.overrides.socialdefense')[0].value);

    let unconsciousrating = Number(document.getElementsByName('system.overrides.unconsciousrating')[0].value);
    let deathrating = Number(document.getElementsByName('system.overrides.deathrating')[0].value);

    let physicalarmor = Number(document.getElementsByName('system.overrides.physicalarmor')[0].value);
    let mysticarmor = Number(document.getElementsByName('system.overrides.mysticarmor')[0].value);

    let recoverytestsrefresh = Number(document.getElementsByName('system.overrides.recoverytestsrefresh')[0].value);
    let recoverytestscurrent = Number(document.getElementsByName('system.overrides.recoverytestscurrent')[0].value);

    let bloodMagicDamage = Number(document.getElementsByName('system.overrides.bloodMagicDamage')[0].value);
    let bloodMagicWounds = Number(document.getElementsByName('system.overrides.bloodMagicWounds')[0].value);

    let woundthreshold = Number(document.getElementsByName('system.overrides.woundthreshold')[0].value);

    let movement = Number(document.getElementsByName('system.overrides.movement')[0].value);

    await this.actor.update({
      'system.overrides.physicaldefense': physicaldefense,
      'system.overrides.mysticdefense': mysticdefense,
      'system.overrides.socialdefense': socialdefense,
      'system.overrides.unconsciousrating': unconsciousrating,
      'system.overrides.deathrating': deathrating,
      'system.overrides.physicalarmor': physicalarmor,
      'system.overrides.mysticarmor': mysticarmor,
      'system.overrides.recoverytestsrefresh': recoverytestsrefresh,
      'system.overrides.recoverytestscurrent': recoverytestscurrent,
      'system.overrides.bloodMagicDamage': bloodMagicDamage,
      'system.overrides.bloodMagicWounds': bloodMagicWounds,
      'system.overrides.woundthreshold': woundthreshold,
      'system.overrides.movement': movement,
    });
  }

  // _attributeChange(ev) {
  //   let pointCost = [-2, -1, 0, 1, 2, 3, 5, 7, 9, 12, 15];
  //   let attribute = $(ev.currentTarget).attr('data-att');
  //   let direction = $(ev.currentTarget).attr('data-direction');
  //   let baseValue = Number(document.getElementsByName(attribute)[0].value);
  //   let attributeAdded = `system.${attribute}added`;
  //   let attributeAddedCurrent = Number(document.getElementsByName(attributeAdded)[0].value);
  //   let attributeAddedNew = attributeAddedCurrent;

  //   if ((attributeAddedCurrent > 7 && direction === 'plus') || (attributeAddedCurrent < -1 && direction === 'minus')) {
  //     ui.notifications.error(game.i18n.localize('earthdawn.c.charGen2LowHigh'));
  //     return false;
  //   } else {
  //     if (direction === 'plus') {
  //       attributeAddedNew = attributeAddedCurrent + 1;
  //     } else if (direction === 'minus') {
  //       attributeAddedNew = attributeAddedCurrent - 1;
  //     }
  //     let newValue = attributeAddedNew + baseValue;
  //     document.getElementsByName(attributeAdded)[0].value = attributeAddedNew;
  //     let attributeTotal = `system.${attribute}total`;
  //     document.getElementsByName(attributeTotal)[0].value = newValue;
  //     let newPointsSpent = Number(document.getElementsByName(attributeAdded)[0].value) + 2;
  //     let totalPoints = pointCost[newPointsSpent];
  //     let attributeSpent = `system.${attribute}points`;
  //     document.getElementsByName(attributeSpent)[0].value = totalPoints;
  //     let totalSpent =
  //       Number(document.getElementsByName('system.dexteritypoints')[0].value) +
  //       Number(document.getElementsByName('system.strengthpoints')[0].value) +
  //       Number(document.getElementsByName('system.toughnesspoints')[0].value) +
  //       Number(document.getElementsByName('system.perceptionpoints')[0].value) +
  //       Number(document.getElementsByName('system.willpowerpoints')[0].value) +
  //       Number(document.getElementsByName('system.charismapoints')[0].value);
  //     document.getElementsByName('system.totalremaining')[0].value = 25 - Number(totalSpent);
  //   }
  // }

  _setChargenAttributes(system) {
    system.dexterityadded = 0;
    system.dexteritypoints = 0;
    system.dexteritytotal = system.namegiver.dexterity;

    system.strengthadded = 0;
    system.strengthpoints = 0;
    system.strengthtotal = system.namegiver.strength;

    system.toughnessadded = 0;
    system.toughnesspoints = 0;
    system.toughnesstotal = system.namegiver.toughness;

    system.perceptionadded = 0;
    system.perceptionpoints = 0;
    system.perceptiontotal = system.namegiver.perception;

    system.willpoweradded = 0;
    system.willpowerpoints = 0;
    system.willpowertotal = system.namegiver.willpower;

    system.charismaadded = 0;
    system.charismapoints = 0;
    system.charismatotal = system.namegiver.charisma;

    system.totalremaining = 25;
  }
}
