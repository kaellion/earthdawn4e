import { ED4E } from '../helpers/config.js';
import { chatOutput } from '../helpers/chat-message.js';
//import { optionBox } from '../helpers/option-box.js';
import { getOptionBox } from '../helpers/option-box.js';
import { karmaAllowed } from '../helpers/karma-allowed.js';
import { devotionAllowed } from '../helpers/devotion-allowed.js';
import { get1eDice } from '../helpers/get-1e-dice.js';
import { get3eDice } from '../helpers/get-3e-Dice.js';
import { getCeDice } from '../helpers/get-ce-dice.js';
import lpSpendingDialog from '../helpers/lp-spending.js';
import { getDice } from '../helpers/get-dice.js';
import { actionTestModifiers, defenseModifiers, damageTestModifiers, initiativeMod } from '../helpers/combat-modifiers.js';
import { rollPrep } from '../helpers/roll-prep.js';
import { explodify } from '../helpers/explodify.js';

export default class earthdawn4eActor extends Actor {
  async _preCreate(data, options, userId) {
    await super._preCreate(data, options, userId);
    this.data.update({
      'system.lpTracking.byInsert': [],
      'system.lpTracking.currentCount': 0,
    });
  }

  /**
   * @override
   * We override this with an empty implementation because we have our own custom way of applying
   * {@link ActiveEffect}s and {@link Actor#prepareEmbeddedDocuments} calls this.
   */
  applyActiveEffects() {
    return;
  }

  prepareData() {
    this.prepareBaseData();
    const baseCharacteristics = [
      'system.attributes.dexterityvalue',
      'system.attributes.strengthvalue',
      'system.attributes.toughnessvalue',
      'system.attributes.perceptionvalue',
      'system.attributes.willpowervalue',
      'system.attributes.charismavalue',
      'system.wounds',
      'system.karma.value',
      'system.bonuses.allRollsStep',
    ];

    const actorData = this;
    if (actorData.type === 'pc') this._prepareCharacterData(actorData);
    if (actorData.type === 'npc') this._prepareNPCData(actorData);
    if (actorData.type === 'creature') this._preparecreatureData(actorData);

    this.overrides = {};
    this._applyBaseEffects(baseCharacteristics);

    switch (actorData.type) {
      case 'pc':
        this.prepareEmbeddedDocuments();
        this._prepareDerivedData(actorData);
        this._applyDerivedEffects(baseCharacteristics);
        this._diceConvert(actorData);
        break;
      case 'npc':
      case 'creature':
        this._applyDerivedEffects(baseCharacteristics);
        break;
    }
  }

  _prepareCharacterData(actorData) {
    const systemData = actorData.system;
    systemData.spells = this.getSpells();
    systemData.movementFinal = Number(systemData.movement) + Number(systemData.overrides.movement);
    systemData.bloodMagicDamage = this.bloodMagicCount() + systemData.overrides.bloodMagicDamage;
    systemData.bloodMagicWounds = 0 + systemData.overrides.bloodMagicWounds;
    systemData.woundsCurrent = systemData.wounds + systemData.bloodMagicWounds;

    systemData.dailytests = Math.ceil(systemData.attributes.toughnessvalue / 6);
    systemData.karma.max = this.karmaCalc() + systemData.unspentattributepoints;
    systemData.devotion.max = this.devotionCalc();
    systemData.bonuses = {};

    /** These bonuses stats are to allow attack actions (which typically aren't part of the Actor) to receive bonuses from Active Effects.
     * They are added to the attack/damage as part of the prep functions
     * Note that, currently, Attack Items aren't considered either Melee or Ranged attacks, and thus won't get bonuses from these
     */
    systemData.bonuses.closeAttack = 0;
    systemData.bonuses.closeDamageStep = 0;
    systemData.bonuses.rangedAttack = 0;
    systemData.bonuses.rangedDamageStep = 0;
    systemData.bonuses.allAttack = 0;
    systemData.bonuses.allDamageStep = 0;
    systemData.bonuses.allRollsStep = 0;
  }

  _applyBaseEffects(baseCharacteristics) {
    let overrides = {};

    // Organize non-disabled effects by their application priority
    //baseCharacteristics is list of attributes that need to have Effects applied before Derived Characteristics are calculated
    const changes = this.effects.reduce((changes, e) => {
      if (e.changes.length < 1) {
        return changes;
      }
      if (e.disabled || e.isSuppressed || !baseCharacteristics.includes(e.changes[0].key)) {
        return changes;
      }

      return changes.concat(
        e.changes.map((c) => {
          c = foundry.utils.duplicate(c);
          c.effect = e;
          c.priority = c.priority ?? c.mode * 10;
          return c;
        }),
      );
    }, []);

    changes.sort((a, b) => a.priority - b.priority);

    // Apply all changes
    for (let change of changes) {
      const result = change.effect.apply(this, change);
      if (result !== null) overrides[change.key] = result[change.key];
    }

    // Expand the set of final overrides
    this.overrides = foundry.utils.expandObject({ ...foundry.utils.flattenObject(this.overrides), ...overrides });
  }

  _prepareDerivedData(actorData) {
    if (actorData.type === 'pc') {
      const systemData = actorData.system;
      this.threadBonusCount();

      let durabilityData = this.getDurability();

      systemData.dexterityStep = this.getStep(systemData.attributes.dexterityvalue);
      systemData.strengthStep = this.getStep(systemData.attributes.strengthvalue);
      systemData.toughnessStep = this.getStep(systemData.attributes.toughnessvalue);
      systemData.perceptionStep = this.getStep(systemData.attributes.perceptionvalue);
      systemData.willpowerStep = this.getStep(systemData.attributes.willpowervalue);
      systemData.charismaStep = this.getStep(systemData.attributes.charismavalue);
      systemData.recoverytestsrefresh = Math.ceil(systemData.attributes.toughnessvalue / 6);

      systemData.unconsciousthreshold = systemData.attributes.toughnessvalue * 2;

      systemData.durability = durabilityData.healthRating;
      systemData.durability2 = systemData.durability;
      systemData.deathThreshold = systemData.unconsciousthreshold + systemData.toughnessStep;
      systemData.unconscious.max =
        systemData.unconsciousthreshold + systemData.durability + systemData.overrides.unconsciousrating - systemData.bloodMagicDamage;
      systemData.damage.max =
        systemData.deathThreshold +
        systemData.durability +
        durabilityData.highestCircle +
        systemData.overrides.deathrating -
        systemData.bloodMagicDamage;

      systemData.unconscious.value = systemData.damage.value;
      systemData.encumbrance = this.carryingCount();

      systemData.recoverytestsrefreshFinal =
        systemData.recoverytestsrefresh + systemData.overrides.recoverytestsrefresh + this.permanentMod('recovery');
      systemData.initiativeStep =
        systemData.dexterityStep -
        this.armorInitiativePenalty() +
        this.permanentMod('initiative') +
        initiativeMod(this) +
        systemData.bonuses.allRollsStep -
        systemData.wounds;
      systemData.physicaldefense =
        Math.floor((systemData.attributes.dexterityvalue + 3) / 2) +
        systemData.overrides.physicaldefense +
        this.permanentMod('physicaldefense') +
        this.defenseCount('physicaldefense') +
        this.physicalDefensemod();
      systemData.mysticdefense =
        Math.floor((systemData.attributes.perceptionvalue + 3) / 2) +
        systemData.overrides.mysticdefense +
        this.permanentMod('mysticdefense') +
        this.defenseCount('mysticdefense') +
        this.mysticDefensemod();
      systemData.socialdefense =
        Math.floor((systemData.attributes.charismavalue + 3) / 2) + this.permanentMod('socialdefense') + systemData.overrides.socialdefense;
      systemData.woundThreshold = Math.ceil((systemData.attributes.toughnessvalue + 4) / 2) + this.permanentMod('woundthreshold');
      systemData.woundThreshold += systemData.overrides.woundthreshold ? systemData.overrides.woundthreshold : 0;
      systemData.carryingCapacity = this.carryingCapacity();
      systemData.physicalarmor =
        this.armorCount('Aphysicalarmor') + +this.permanentMod('physicalarmor') + systemData.overrides.physicalarmor;
      systemData.mysticarmor =
        this.armorCount('Amysticarmor') +
        this.permanentMod('mysticarmor') +
        Math.floor(systemData.attributes.willpowervalue / 5) +
        systemData.overrides.mysticarmor;
    }
  }

  _applyDerivedEffects(baseCharacteristics) {
    const overrides = {};

    // Organize non-disabled effects by their application priority
    const changes = this.effects.reduce((changes, e) => {
      if (e.changes.length < 1) {
        return changes;
      }
      if (e.disabled || e.isSuppressed || baseCharacteristics.includes(e.changes[0].key)) {
        return changes;
      }

      return changes.concat(
        e.changes.map((c) => {
          c = foundry.utils.duplicate(c);
          c.effect = e;
          c.priority = c.priority ?? c.mode * 10;
          return c;
        }),
      );
    }, []);

    changes.sort((a, b) => a.priority - b.priority);

    // Apply all changes
    for (let change of changes) {
      const result = change.effect.apply(this, change);
      if (result !== null) overrides[change.key] = result[change.key];
    }

    // Expand the set of final overrides
    this.overrides = foundry.utils.expandObject({ ...foundry.utils.flattenObject(this.overrides), ...overrides });
  }

  _diceConvert(actorData) {
    const systemData = actorData.system;
    systemData.dexterityDice = getDice(systemData.dexterityStep);
    systemData.strengthDice = getDice(systemData.strengthStep);
    systemData.toughnessDice = getDice(systemData.toughnessStep);
    systemData.perceptionDice = getDice(systemData.perceptionStep);
    systemData.willpowerDice = getDice(systemData.willpowerStep);
    systemData.charismaDice = getDice(systemData.charismaStep);
    systemData.initiativeDice = getDice(systemData.initiativeStep);
    systemData.initiativeRoll = explodify(systemData.initiativeDice);
    systemData.unconsciousleft.max = systemData.unconscious.max;
    systemData.unconsciousleft.value = systemData.unconscious.max - systemData.damage.value;
  }

  _prepareNPCData(actorData) {
    const systemData = actorData.system;
    systemData.bonuses = {};

    /** These bonuses stats are to allow attack actions (which typically aren't part of the Actor) to receive bonuses from Active Effects.
     * They are added to the attack/damage as part of the prep functions
     * Not that, currently, Attack Items aren't considered either Melee or Ranged attacks, and thus won't get bonuses from these
     */
    systemData.bonuses.closeAttack = 0;
    systemData.bonuses.closeDamageStep = 0;
    systemData.bonuses.rangedAttack = 0;
    systemData.bonuses.rangedDamageStep = 0;
    systemData.bonuses.allAttack = 0;
    systemData.bonuses.allDamageStep = 0;
    systemData.bonuses.allRollsStep = 0;
    systemData.modifiedInitiative =
      Number(systemData.initiativeStep) - Number(systemData.wounds) + Number(initiativeMod(this)) + Number(systemData.bonuses.allRollsStep);
    systemData.initiativeDice = getDice(systemData.modifiedInitiative);
    systemData.initiativeRoll = explodify(systemData.initiativeDice);
    systemData.damage.max = systemData.deathThreshold;
    systemData.unconscious.max = systemData.unconsciousThreshold;
    systemData.dexterityDice = getDice(systemData.dexterityStep);
    systemData.strengthDice = getDice(systemData.strengthStep);
    systemData.toughnessDice = getDice(systemData.toughnessStep);
    systemData.perceptionDice = getDice(systemData.perceptionStep);
    systemData.willpowerDice = getDice(systemData.willpowerStep);
    systemData.charismaDice = getDice(systemData.charismaStep);
    systemData.unconsciousleft.max = systemData.unconsciousThreshold;
    systemData.unconsciousleft.value = systemData.unconsciousThreshold - systemData.damage.value;
    systemData.woundsCurrent = systemData.wounds;
    this.prepareEmbeddedDocuments();
  }

  _preparecreatureData(actorData) {
    const systemData = actorData.system;
    systemData.bonuses = {};
    systemData.bonuses.allRollsStep = 0;
    systemData.modifiedInitiative =
      Number(systemData.initiativeStep) + Number(initiativeMod(this)) + Number(systemData.wounds) + Number(systemData.bonuses.allRollsStep);
    //console.log(systemData.modifiedInitiative);
    systemData.initiativeDice = getDice(systemData.modifiedInitiative);
    systemData.initiativeRoll = explodify(systemData.initiativeDice);
    systemData.damage.max = systemData.deathThreshold;
    systemData.unconscious.max = systemData.unconsciousThreshold;
    systemData.unconsciousleft.max = systemData.unconsciousThreshold;
    systemData.unconsciousleft.value = systemData.unconsciousThreshold - systemData.damage.value;
  }

  threadBonusCount() {
    const systemData = this;
    const talents = systemData.items.filter(function (item) {
      return item.type === 'talent';
    });
    const threads = systemData.items.filter(function (item) {
      return item.type === 'thread' && item.system.active === true;
    });
    for (const element of talents) {
      let matchingThread = threads.filter(function (thread) {
        return thread.system.characteristic === element.id;
      });
      let talentBonus = 0;
      if (matchingThread.length > 0) {
        talentBonus = matchingThread[0].system.rank;
      }
      element.system.finalranks = Number(element.system.ranks) + Number(talentBonus);
    }
  }

  armorCount(type) {
    const systemData = this;
    const armor = systemData.items.filter(function (item) {
      return item.type === 'armor' && item.system.worn === true;
    });

    let runningtotal = 0;

    let namegiver = this.items.filter(function (item) {
      return item.type === 'namegiver';
    });
    if (namegiver.length > 0 && game.i18n.localize(namegiver[0].system.name) === 'Obsidiman' && type === 'Aphysicalarmor') {
      runningtotal += 3;
    }
    for (const element of armor) {
      if (type === 'Aphysicalarmor') {
        runningtotal += element.system.physicalArmorFinal;
      } else if (type === 'Amysticarmor') {
        runningtotal += element.system.mysticArmorFinal;
      }
    }

    return runningtotal;
  }

  armorInitiativePenalty() {
    const systemData = this;
    const armor = systemData.items.filter(function (item) {
      return item.type === 'armor' && item.system.worn === true;
    });
    const shield = systemData.items.filter(function (item) {
      return item.type === 'shield' && item.system.worn === true;
    });
    let penalty = 0;
    if (armor.length > 0) {
      penalty += armor[0].system.armorPenalty;
    }
    if (shield.length > 0) {
      penalty += shield[0].system.initiativepenalty;
    }

    return penalty;
  }

  karmaCalc() {
    const systemData = this;
    const namegiver = systemData.items.filter(function (item) {
      return item.type === 'namegiver';
    });
    let karmamod = 0;
    if (namegiver.length > 0) {
      karmamod = namegiver[0].system.karmamodifier;
    }
    const disciplines = this.items.filter(function (item) {
      return item.type === 'discipline';
    });
    let totalcircles = 0;

    for (const element of disciplines) {
      let discCircle = Number(element.system.circle);
      if (discCircle > totalcircles) {
        totalcircles = discCircle;
      }
    }

    let circles = totalcircles;
    return Number(circles * karmamod);
  }

  devotionCalc() {
    const data = this.system;
    const questor = this.items.filter(function (item) {
      return item.type === 'discipline';
    });
    let rank = 0;
    for (let i = 0; i < questor.length; i++) {
      if (questor[i].system.discipline === 'questor') {
        let discrank = questor[i].system.circle;
        rank = discrank;
      }
    }
    let questorrank = rank;
    return Number(questorrank * 10);
  }

  defenseCount(type) {
    const systemData = this;
    const shield = systemData.items.filter(function (item) {
      return item.type === 'shield' && item.system.worn === true;
    });
    let runningtotal = 0;
    let shieldvalue = 0;

    for (const element of shield) {
      shieldvalue = Number(element.system[type]);
      runningtotal += shieldvalue;
    }

    return runningtotal;
  }

  permanentMod(input) {
    const systemData = this;
    const namegiver = systemData.items.filter(function (item) {
      return item.type === 'namegiver';
    });
    let runningtotal = 0;
    for (let i = 0; i < namegiver.length; i++) {
      if (namegiver[i].system.bonus1.characteristic === input) runningtotal += namegiver[i].system.bonus1.value;
    }
    const disciplines = systemData.items.filter(function (item) {
      return item.type === 'discipline' && item.system.discipline !== 'path';
    });
    const paths = systemData.items.filter(function (item) {
      return item.type === 'discipline' && item.system.discipline === 'path';
    });

    let workingdiscipline;
    let disciplineBonus = 0;
    let workingpath;
    let p;
    let modifiedDisciplineName;
    let pathModifier = 0;
    let totalCircles = 0;

    // get any bonus from Path for INPUT
    for (p = 0; p < paths.length; p++) {
      workingpath = paths[p];
      modifiedDisciplineName = workingpath.system.relatedDiscipline;
      var r;
      for (r = 1; r <= workingpath.system.circle; r++) {
        let characteristic = getProperty(workingpath, `system.circle${r}.characteristic`);
        if (characteristic === input && workingpath.system['circle' + r].value > pathModifier) {
          pathModifier = workingpath.system['circle' + r].value;
        }
      }
    }

    runningtotal += pathModifier;

    // get any bonus from Discipline for INPUT
    let d;
    for (d = 0; d < disciplines.length; d++) {
      workingdiscipline = disciplines[d];
      let modifiedValue;
      var r;
      for (r = 1; r <= workingdiscipline.system.circle; r++) {
        let characteristic = getProperty(workingdiscipline, `system.circle${r}.characteristic`);

        if (characteristic === input) {
          modifiedValue = workingdiscipline.system['circle' + r].value;
          if (characteristic === input && modifiedValue > disciplineBonus) {
            disciplineBonus = modifiedValue;
          }
        }
      }
    }

    runningtotal += disciplineBonus;

    // Get any bonus from Thread Items for INPUT
    const threadItem = systemData.items.filter(function (item) {
      return (
        (item.type === 'weapon' || item.type === 'shield' || item.type === 'armor' || item.type === 'equipment') &&
        item.system.numberthreads > 0
      );
    });

    for (let i = 0; i < threadItem.length; i++) {
      let ranks = threadItem[i].system.numberthreads;
      let r;
      for (r = 1; r <= ranks; r++) {
        let rankKey = `rank${r}`;
        let threadRank = threadItem[i].system.threads[rankKey];

        if (threadRank.threadactive === true && threadRank.characteristic === input) {
          runningtotal += Number(threadRank.value);
        }
      }
    }

    // Get any bonus from Threads for INPUT
    const threads = systemData.items.filter(function (item) {
      return item.type === 'thread' && item.system.active === true;
    });

    for (let i = 0; i < threads.length; i++) {
      if (threads[i].system.characteristic === input) runningtotal += Number(threads[i].system.rank);
    }

    return runningtotal;
  }
  /**
   * must be the name as given in the actor data under tactics. only true for boolean values
   * @returns {boolean} new value of the changed modifier or undefined if modifier doesn't exist.
   * @param {String} modifierName
   */
  toggleTacticsModifier(modifierName) {
    if (!this.system.tactics.hasOwnProperty(modifierName)) {
      console.warn(`system.tactics does not have property ${modifierName}`);
      return undefined;
    } else {
      this.system.tactics[modifierName] = !this.system.tactics[modifierName];
      return this.system.tactics[modifierName];
    }
  }

  physicalDefensemod() {
    let mod = this.getDefenseModifiers();
    return mod;
  }

  mysticDefensemod() {
    let mod = this.getDefenseModifiers();
    return mod;
  }

  carryingCount() {
    const systemData = this;
    const stuff = systemData.items.filter(function (item) {
      return item.type === 'weapon' || item.type === 'armor' || item.type === 'shield' || item.type === 'equipment';
    });
    let runningtotal = 0;
    var i;
    for (i = 0; i < stuff.length; i++) {
      if (stuff[i].system.weight > 0) {
        runningtotal += Number(stuff[i].system.weight);
      }
    }
    return runningtotal;
  }

  bloodMagicCount() {
    const systemData = this;
    const stuff = systemData.items.filter(function (item) {
      return item.type === 'weapon' || item.type === 'armor' || item.type === 'shield' || item.type === 'equipment';
    });
    let runningtotal = 0;
    var i;
    for (i = 0; i < stuff.length; i++) {
      if (stuff[i].system.bloodMagicDamage > 0) {
        runningtotal += Number(stuff[i].system.bloodMagicDamage);
      }
    }
    return runningtotal;
  }

  carryingCapacity() {
    let namegiver = this.items.filter(function (item) {
      return item.type === 'namegiver';
    });
    let a = 0;
    let s = this.system.attributes.strengthvalue;
    if (namegiver.length > 0 && this.permanentMod('carryingcapacity')) {
      s += 2;
    }
    let t = Math.ceil(s / 5);
    return -12.5 * t ** 2 + 5 * t * s + 12.5 * t + 5;
  }

  getDice(step) {
    let dice = 0;
    if (game.settings.get('earthdawn4e', 'stepTableEdition') === 'step4') {
      var stepTable = [
        '0',
        '1d4-2',
        '1d4-1',
        '1d4',
        '1d6',
        '1d8',
        '1d10',
        '1d12',
        '2d6',
        '1d8+1d6',
        '2d8',
        '1d10+1d8',
        '2d10',
        '1d12+1d10',
        '2d12',
        '1d12+2d6',
        '1d12+1d8+1d6',
        '1d12+2d8',
        '1d12+1d10+1d8',
        '1d20+2d6',
        '1d20+1d8+1d6',
      ];
      if (step < 0) {
        return;
      } else if (step < 19) {
        dice = stepTable[step];
      } else {
        let i = step;
        let loops = 0;
        while (i > 18) {
          loops += 1;
          i -= 11;
        }
        dice = loops + 'd20+' + stepTable[i];
      }
    } else if (game.settings.get('earthdawn4e', 'stepTableEdition') === 'step1') {
      dice = get1eDice(step);
    } else if (game.settings.get('earthdawn4e', 'stepTableEdition') === 'step3') {
      dice = get3eDice(step);
    } else if (game.settings.get('earthdawn4e', 'stepTableEdition') === 'stepC') {
      dice = getCeDice(step);
    }
    return dice;
  }

  getStep(value) {
    if (!value > 0) {
      return 0;
    } else {
      return Number([Math.ceil(value / 3) + 1]);
    }
  }

  getDurability() {
    const systemData = this;

    const disciplines = systemData.items.filter(function (item) {
      return item.type === 'discipline';
    });

    let runningtotal = 0;
    var discCircle = 0;
    var discDura = 0;

    // ERRATTA https://docs.google.com/document/d/1ts_bx53HtB5-ThUxzHFsCPM4-g9wjIzHdJLdGy4XcfY/edit#heading=h.bx1zxh37l1bb
    // Durability Improvement with Thread Magic, Page 230
    // Clarification:
    // When Durability is improved through the use of thread magic(e.g.pattern items or thread items),
    // the adept’s highest Circle is effectively increased by the bonus for determining their Durability.
    // For example, Beispeil has a Band of the Elements, which gives them + 1 Durability.
    // Their highest Circle is in Example (Circle 7).
    // This improves them to Circle 8 for the purposes of determining their Durability.

    let duraCircleMod = Number(this.permanentMod('durability'));

    // find the highest circle
    let highest = { id: '', circle: 0, durability: 0 };
    for (const element of disciplines) {
      let circle = Number(element.system.circle);
      let durability = Number(element.system.durability);

      // save some data as we try to determine the highest circle
      // highest circle is disc with the highest circle and the largest durability
      // in case of a tie
      if (circle > highest.circle) {
        highest.id = element._id;
        highest.circle = circle;
        highest.durability = durability;
      } else if (circle === highest.circle && durability > highest.durability) {
        highest.id = element._id;
        highest.durability = durability;
      }
    }

    // operate on the highest durablity first as it overrides a lower durability
    disciplines.sort((a, b) => (a.system.durability > b.system.durability ? -1 : 1));

    for (const element of disciplines) {
      let circle = Number(element.system.circle);

      // if this discipline matches the one tagged as the highest circle discipline, apply the duraCircleMod
      circle = element._id === highest.id ? circle + duraCircleMod : circle;

      if (circle - discCircle > 0) {
        discCircle = circle - discCircle;
        discDura = Number(element.system.durability);
      } else {
        discDura = 0;
      }

      let total = discDura * discCircle;
      runningtotal += total;
    }
    return { healthRating: runningtotal, highestCircle: highest.circle };
  }

  getModifiedDefense(defenseType) {
    // TODO: error handling, wrong parameter
    let modDefense = this.system[defenseType] ?? 0;
    if (this.type !== 'pc') {
      modDefense += this.getDefenseModifiers();
    }
    return modDefense;
  }

  getDefenseModifiers() {
    let mod = 0;
    /* a knocked down character cannot use any combat options with the exception of jump up */
    if (this.system.tactics.knockeddown === true) {
      mod -= 3;
    } else {
      if (this.system.tactics.aggressive === true) {
        mod -= 3;
      } else if (this.system.tactics.defensive === true) {
        mod += 3;
      }
    }

    if (this.system.tactics.harried === true) {
      mod -= 2;
    }

    return mod;
  }

  getSpells() {
    const spells = this.items.filter(function (item) {
      return item.type === 'spell';
    });
  }

  getAttack(weapontype) {
    let attacktype = weapontype;
    let attackname = '';
    if (attacktype === 'Melee') {
      attackname = game.i18n.localize('earthdawn.m.melee_weapon');
    } else if (attacktype === 'Ranged') {
      attackname = game.i18n.localize('earthdawn.m.missile_weapon');
    } else if (attacktype === 'Thrown') {
      attackname = game.i18n.localize('earthdawn.t.thrown_weapon');
    } else if (attacktype === 'Unarmed') {
      attackname = game.i18n.localize('earthdawn.u.unarmed_combat');
    }

    let attack = this.items.filter(function (item) {
      return item.name === `${attackname}` && item.type === 'talent';
    });
    if (attack.length < 1) {
      attack = this.items.filter(function (item) {
        return item.name === `${attackname}` && item.type === 'skill';
      });
    }
    if (attack.length < 1) {
      return;
    } else {
      return attack[0].id;
    }
  }

  async halfMagic() {
    const inputs = {
      wounds: this.system.woundsCurrent,
    };
    const disciplines = this.items.filter(function (item) {
      return item.type === 'discipline';
    });
    var disciplinelist = `
    <label class='misc-mod-description'>${game.i18n.localize('earthdawn.d.discipline')}</label>
    <select class='misc-mod-input' id='discipline_box'>
    `;
    var i;
    var disciplineId;
    var disciplineName;
    for (i = 0; i < disciplines.length; i++) {
      disciplineId = disciplines[i]._id;
      disciplineName = disciplines[i].name;
      disciplinelist += `<option value='${disciplineId}'>${disciplineName}</option>`;
    }
    disciplinelist += '</select>';

    const html = `
        <div>
            <div class='misc-mod'>
                <label class='misc-mod-description'>${game.i18n.localize('earthdawn.a.attribute')}: </label>
                <select id='attribute_box'>
                    <option value='dexterityStep'>Dexterity</option>
                    <option value='strengthStep'>Strength</option>
                    <option value='toughnessStep'>Toughness</option>
                    <option value='perceptionStep'>Perception</option>
                    <option value='willpowerStep'>Willpower</option>
                    <option value='charismaStep'>Charisma</option>
                </select>
            </div>
            <div class='misc-mod'>${disciplinelist}</div>
            <div class='misc-mod'>
                <label class='misc-mod-description'>${game.i18n.localize('earthdawn.m.modifierStep')}: </label>
                <input class='misc-mod-input circleInput' id='dialog_box' value='0' autofocus/>
            </div>
            <div class='misc-mod'>
                <label class='misc-mod-description'>${game.i18n.localize('earthdawn.d.difficulty')}: </label>
                <input class='misc-mod-input circleInput' id='difficulty_box' value='0' data-type='number' />
            </div>
        </div>
        <div >
            <div class='misc-mod'>
                <label class='misc-mod-description'>${game.i18n.localize('earthdawn.k.karma')}: </label>
                <input class='misc-mod-input circleInput' id='karma_box' value='0' data-type='number' />
            </div>
        </div>
        <div >
            <div class='misc-mod'>
                <label class='misc-mod-description'>${game.i18n.localize('earthdawn.d.devotion')}: </label>
                <input class='misc-mod-input circleInput' id='devotion_box' value='0' data-type='number' />
            </div>
        </div>
        <div>
            <select name='system.devotionDie' id='devotionDie_box' value='${inputs.devotionDie}' {{#select system.devotionDie}}>
                <option value='na'>na</option>
                <option value='d4'>${game.i18n.localize('earthdawn.d.d')}4</option>
                <option value='d6'>${game.i18n.localize('earthdawn.d.d')}6</option>
                <option value='d8'>${game.i18n.localize('earthdawn.d.d')}8</option>
                {{/select}}
            </select>
        </div>
        `;
    let miscmod = await new Promise((resolve) => {
      const sheet = this._sheet;
      new Dialog({
        title: game.i18n.localize('earthdawn.m.miscellaneousModifiers'),
        content: html,
        buttons: {
          ok: {
            label: game.i18n.localize('earthdawn.o.ok'),
            callback: (html) => {
              resolve({
                attribute: html.find('#attribute_box').val(),
                discipline: html.find('#discipline_box').val(),
                modifier: html.find('#dialog_box').val(),
                difficulty: html.find('#difficulty_box').val(),
                karma: html.find('#karma_box').val(),
                devotion: html.find('#devotion_box').val(),
                devotionDie: html.find('#devotionDie_box').val(),
              });
            },
          },
        },
        default: 'ok',
      }).render(true);
    });

    let activediscipline = this.items.get(`${miscmod.discipline}`);
    let circle = Number(activediscipline.system.circle);
    let attributestep = Number(this.system[miscmod.attribute]);
    let basestep = Number(circle + attributestep + Number(miscmod.modifier));

    inputs.talent = `${game.i18n.localize('earthdawn.h.halfmagic')} (${activediscipline.name})`;
    inputs.karma = miscmod.karma;
    inputs.devotion = miscmod.devotion;
    inputs.devotionDie = miscmod.devotionDie;

    inputs.steps = basestep + Number(miscmod.modifier);
    console.log('[EARTHDAWN] HALFMAGIC TEST', inputs);
    this.rollPrep(inputs);
    console.log('[EARTHDAWN] HALFMAGIC TEST', inputs);
  }

  async knockdownTest(inputs) {
    let result = {};
    if (this.system.tactics.knockeddown === true) {
      ui.notifications.info(`${game.i18n.localize('earthdawn.c.combatModAlreadyKnockeddown')}`);
      return;
    }
    if (this.type !== 'pc') {
      inputs.difficulty = inputs.difficulty ? inputs.difficulty : 0;
      inputs.strain = inputs.strain ? inputs.strain : 0;
      inputs.karma = inputs.karma ? inputs.karma : 0;
      inputs.modifier = inputs.modifier ? inputs.modifier : 0;
      inputs.devotion = inputs.devotion ? inputs.devotion : 0;
      inputs = {
        finalstep: this.system.knockdown,
        rolltype: 'knockdown',
        talent: 'Knockdown',
        difficulty: inputs.difficulty,
      };
      result = await rollPrep(this, inputs);
    } else {
      let difficulty = inputs.difficulty ? inputs.difficulty : 0;
      inputs = {
        attribute: 'strengthStep',
        rolltype: 'knockdown',
        name: 'Knockdown',
        difficulty: difficulty,
        title: game.i18n.localize('earthdawn.k.knockdownTest'),
      };
      result = await rollPrep(this, inputs);
    }
    console.log('[EARTHDAWN] Knockdown Test', inputs, result);
    if (result.margin < 0) {
      this.update({ 'system.tactics.knockeddown': true });
      this.update({ 'system.tactics.aggressive': false });
      this.update({ 'system.tactics.defensive': false });
    }
  }

  async jumpUpTest(inputs) {
    inputs = { attribute: 'dexterityStep', name: 'Jump Up', difficulty: 6, strain: 2, rolltype: 'jumpUp' };
    //console.log(this);
    let results = await rollPrep(this, inputs);
    //console.log(this);
    if (results.margin >= 0) {
      this.update({ 'system.tactics.knockeddown': false });
    } else if (results.margin < 0) {
      this.update({ 'system.tactics.knockeddown': true });
      this.update({ 'system.tactics.aggressive': false });
      this.update({ 'system.tactics.defensive': false });
    }
  }

  async attuneMatrix(matrix) {
    let spellList = this.items.filter(function (item) {
      return item.type === 'spell' && item.system.circle <= matrix.system.circle;
    });
    let spellOptions = '';
    var arrayLength = spellList.length;
    for (var i = 0; i < arrayLength; i++) {
      let id = spellList[i].id;
      let name = spellList[i].name;
      spellOptions += `<option value='${id}'>${name}</option>`;
    }

    let html =
      '<select id= "spell-name" name="newspell">' +
      spellOptions +
      '</select><div><input type="checkbox" id="attune_fly">' +
      game.i18n.localize('earthdawn.a.attuneOnFly') +
      '</div>';

    let newspell = await new Promise((resolve) => {
      new Dialog({
        title: game.i18n.localize('earthdawn.m.matrixAttune'),
        content: html,
        buttons: {
          ok: {
            label: game.i18n.localize('earthdawn.o.ok'),
            callback: (html) => {
              resolve({
                id: html.find('#spell-name').val(),
                onfly: html.find('#attune_fly:checked'),
              });
            },
          },
          cancel: {
            label: game.i18n.localize('earthdawn.c.cancel'),
            callback: () => {},
          },
        },
        default: 'ok',
      }).render(true);
    });
    let spellarray = this.items.filter(function (item) {
      return item.id === `${newspell.id}`;
    });
    let spellinfo = spellarray[0];
    let discipline = this.getWeaving(spellinfo.system.discipline);
    discipline = game.i18n.localize(discipline);
    let itemarray = this.items.filter(function (item) {
      return item.name.includes(`${discipline}`) && item.type === 'talent';
    });
    if (itemarray.length < 1) {
      ui.notifications.error(
        `${game.i18n.localize('earthdawn.y.youDoNotHave')} ${discipline} - ${game.i18n.localize('earthdawn.a.attuneNot')}`,
      );
      return false;
    }
    let itemID = itemarray[0].id;
    if (newspell.onfly.length > 0) {
      let inputs = {
        difficulty: spellinfo.system.reattunedifficulty,
        talentID: itemID,
      };
      let strainInputs = { damage: 1, ignorearmor: true, type: 'physical' };
      await this.takeDamage(strainInputs);
      let returnvalue = await rollPrep(this, inputs);
      if (returnvalue.margin > 0) {
        matrix.update({
          'system.threadsrequired': spellinfo.system.threadsrequired,
          'system.targetadditionalthreads': 0,
          'system.spellId': spellinfo._id,
          'system.range': spellinfo.system.range,
          'system.currentspell': spellinfo.name,
          'system.weavingdifficulty': spellinfo.system.weavingdifficulty,
          'system.castingdifficulty': spellinfo.system.castingdifficulty,
          'system.activethreads': 0,
          'system.discipline': spellinfo.system.discipline,
          'system.extrathreads': spellinfo.system.extrathreads,
          'system.extrasuccesses': spellinfo.system.extrasuccesses,
        });
      } else {
        ui.notifications.info(`${game.i18n.localize('earthdawn.a.attuneFailed')}`);
      }
    } else {
      matrix.update({
        'system.threadsrequired': spellinfo.system.threadsrequired,
        'system.targetadditionalthreads': 0,
        'system.spellId': spellinfo._id,
        'system.range': spellinfo.system.range,
        'system.currentspell': spellinfo.name,
        'system.weavingdifficulty': spellinfo.system.weavingdifficulty,
        'system.castingdifficulty': spellinfo.system.castingdifficulty,
        'system.activethreads': 0,
        'system.discipline': spellinfo.system.discipline,
        'system.extrathreads': spellinfo.system.extrathreads,
        'system.extrasuccesses': spellinfo.system.extrasuccesses,
      });
    }
  }

  async weaveThread(matrix) {
    let thread_weaving = this.getWeaving(matrix.system.discipline);
    let localizedDiscipline = 'earthdawn.d.discipline' + matrix.system.discipline;
    let discipline_fallback = getProperty(game.i18n._fallback, `${localizedDiscipline}`);
    localizedDiscipline = game.i18n.localize(localizedDiscipline);
    discipline_fallback = discipline_fallback === null ? game.i18n.localize(localizedDiscipline) : discipline_fallback;
    let thread_weaving_fallback = getProperty(game.i18n._fallback, `${thread_weaving}`);
    thread_weaving_fallback = thread_weaving_fallback === null ? game.i18n.localize(thread_weaving) : thread_weaving_fallback;
    thread_weaving = game.i18n.localize(thread_weaving);
    let disciplineinfo = this.items.filter(function (item) {
      return item.type === `discipline` && (item.name.includes(`${localizedDiscipline}`) || item.name.includes(`${discipline_fallback}`));
    });
    let talent = this.items.filter(function (item) {
      return item.type === `talent` && (item.name.includes(`${thread_weaving}`) || item.name.includes(`${thread_weaving_fallback}`));
    });
    if (talent.length < 1) {
      ui.notifications.info(`${game.i18n.localize('earthdawn.y.youDoNotHave')} ` + thread_weaving + ' or ' + thread_weaving_fallback);
      return false;
    }

    let maxextra = 0;
    if (disciplineinfo.length > 0) {
      let label = game.i18n.localize('earthdawn.e.extraThreads');
      let label2 = game.i18n.localize('earthdawn.e.extraThreadEffects');
      let label3 = game.i18n.localize('earthdawn.e.extraThreadRemember');
      let label4 = game.i18n.localize('earthdawn.e.extra');
      if (matrix.system.targetadditionalthreads === 0 || matrix.system.targetadditionalthreads === null) {
        if (disciplineinfo.length > 0) {
          maxextra = Math.ceil(disciplineinfo[0].system.circle / 4);
        }
        let extraThreads = matrix.system.extrathreads;
        extraThreads = extraThreads.replace(/,/g, '</li><li>');
        extraThreads = '<ul><li>' + extraThreads + '</li></ul>';

        let html = `<div class='weave-thread-box'>
                      <label>${label}</label>
                      <input class='circleInput' type='text' id='extra-threads' name='extra-threads' data-dtype='Number' />

                      <br/>${label3} ${maxextra} ${label4}<br/>
                      <label>${label2}:</label>
                      ${extraThreads}
                    </div>`;

        let threadextra = await new Promise((resolve) => {
          new Dialog({
            title: game.i18n.localize('earthdawn.m.matrixWeave'),
            content: html,
            buttons: {
              ok: {
                label: game.i18n.localize('earthdawn.o.ok'),
                callback: (html) => {
                  resolve({
                    extrathreads: html.find('#extra-threads').val(),
                  });
                },
              },
              cancel: {
                label: game.i18n.localize('earthdawn.c.cancel'),
                callback: () => {},
              },
            },
            default: 'ok',
          }).render(true);
        });
        threadextra.extrathreads = threadextra.extrathreads > maxextra ? maxextra : threadextra.extrathreads;
        await matrix.update({ 'system.targetadditionalthreads': Number(threadextra.extrathreads) });
      }
    }
    let parameters = { talentID: talent[0].id, type: 'talent', difficulty: matrix.system.weavingdifficulty };
    let results = await rollPrep(this, parameters);
    if (Number(results.margin) >= 0) {
      let targetthreads = Number(matrix.system.threadsrequired) + Number(matrix.system.targetadditionalthreads);
      if (matrix.system.activethreads < targetthreads) {
        let newthreads = Number(matrix.system.activethreads) + 1 + Number(results.extraSuccess);
        newthreads = newthreads > targetthreads ? targetthreads : newthreads;
        await matrix.update({ 'system.activethreads': Number(newthreads) });
      }
    }
  }

  async castSpell(matrix) {
    if (matrix.data.data.totalthreadsneeded > matrix.data.data.totalthreads) {
      ui.notifications.info(`${game.i18n.localize('earthdawn.t.threadsNotEnough')} `);
      return false;
    } else {
      let talent = this.items.filter(function (item) {
        return item.type === `talent` && (item.name === `Spellcasting` || item.name === `Spruchzauberei`);
      });
      if (talent.length < 1) {
        ui.notifications.info(`${game.i18n.localize('earthdawn.s.spellcastingMissing')} `);
        return;
      }
      var difficulty;
      const targets = Array.from(game.user.targets);
      if (
        (matrix.system.castingdifficulty.includes('TMD') ||
          matrix.system.castingdifficulty.includes('MVZ') ||
          matrix.system.castingdifficulty.includes('MWST') ||
          matrix.system.castingdifficulty.includes('MVK')) &&
        matrix.system.range === 'Self'
      ) {
        difficulty = this.system.mysticdefense;
      } else if (
        (matrix.system.castingdifficulty === 'TMD' ||
          matrix.system.castingdifficulty === 'MVZ' ||
          matrix.system.castingdifficulty === 'MWST' ||
          matrix.system.castingdifficulty === 'MVK') &&
        targets.length > 0
      ) {
        difficulty = targets[0].actor.system.mysticdefense;
        if (targets[0].actor.type !== 'pc') {
          difficulty += defenseModifiers(targets[0].actor);
        }
      } else if (!isNaN(matrix.system.castingdifficulty)) {
        difficulty = matrix.system.castingdifficulty;
      } else {
        difficulty = 0;
      }

      let inputs = {
        spellName: matrix.system.currentspell,
        talentID: talent[0].id,
        rolltype: 'spell',
        type: talent[0].type,
        difficulty: difficulty,
        spellId: matrix.system.spellId,
        targets: targets,
      };
      //console.log(inputs);
      let results = await rollPrep(this, inputs);
      if (results) {
        await matrix.update({ 'system.activethreads': 0, 'system.targetadditionalthreads': 0 });
      }
    }
  }

  rollPrep(inputs) {
    //this function exists for backward compatability, the real action now happens in /helpers/roll-prep.js
    rollPrep(this, inputs);
  }

  getWeaving(discipline) {
    let weavingTalent = '';

    switch (discipline) {
      case 'Elementalist':
        weavingTalent = 'earthdawn.e.elementalism';
        return weavingTalent;
      case 'Illusionist':
        weavingTalent = 'earthdawn.i.illusionism';
        return weavingTalent;
      case 'Nethermancer':
        weavingTalent = 'earthdawn.n.nethermancy';
        return weavingTalent;
      case 'Wizard':
        weavingTalent = 'earthdawn.w.wizardry';
        return weavingTalent;
      case 'Shaman':
        weavingTalent = 'earthdawn.s.shamanism';
        return weavingTalent;
    }
  }

  clearMatrix(matrix) {
    matrix.update({
      'system.currentspell': '',
      'system.weavingdifficulty': '',
      'system.targetadditionalthreads': '',
      'system.castingdifficulty': '',
      'system.activethreads': '',
      'system.threadsrequired': '',
    });
  }

  async weaponDamagePrep(weapon, extraSuccess, damageBonus) {
    let damage = '';
    let step = '';
    let damageStep = 0;
    let damageBonus2 = damageBonus;
    let rollType = '';

    if (weapon.type === 'weapon') {
      damage = weapon.system.damageattribute;
      step = damage + 'Step';
      if (weapon.system.weapontype === 'Ranged' || weapon.system.weapontype === 'Thrown') {
        rollType = 'weapondamageRanged';
      } else {
        rollType = 'weapondamage';
      }
    } else if (weapon.type === 'attack') {
      if (weapon.system.powerType === 'Attack') {
        damageStep = weapon.system.damagestep;
        rollType = 'attackdamage';
      } else {
        rollType = 'testdamage';
        damageStep = weapon.system.damagestep;
      }
    } else if (weapon.type === 'talent') {
      damage = weapon.system.attribute;
      step = damage;
      damageStep = weapon.system.final;
    }
    console.log('ROLLTYPE: ' + rollType);
    let att = this.system[step] ? Number(this.system[step]) : 0;
    if (weapon.type === 'talent') {
      damageStep = att + Number(weapon.system.finalranks);
      if (weapon.name === game.i18n.localize('Claw Shape')) {
        damageStep += 3;
      }
    } else if (weapon.type === 'weapon') {
      if (weapon.system.weapontype === 'Melee' || weapon.system.weapontype === 'Unarmed') {
        damageStep =
          att + Number(weapon.system.damageFinal) + Number(this.system.bonuses.closeDamageStep) + Number(this.system.bonuses.allDamageStep);
      } else if (weapon.system.weapontype === 'Ranged' || weapon.system.weapontype === 'Thrown') {
        damageStep =
          att +
          Number(weapon.system.damageFinal) +
          Number(this.system.bonuses.rangedDamageStep) +
          Number(this.system.bonuses.allDamageStep);
      }
    }
    let name = weapon.name;
    
      if (damageStep < 1) {
        damageStep = 1;
      }
    
    let inputs = {
      rolltype: rollType,
      damagestep: damageStep,
      extraSuccess: extraSuccess,
      talent: `${game.i18n.localize('earthdawn.d.damage')} (${name})`,
      damageBonus: damageBonus2,
    };
    this.effectTest(inputs);
  }

  async effectTest(inputs) {
    let basestep = inputs.damagestep;
    let extraSuccess = 0;
    if (inputs.extraSuccess !== null) {
      extraSuccess = Number(inputs.extraSuccess * 2);
      if (inputs.damageBonus && inputs.damageBonus !== null) {
        extraSuccess += Number(inputs.damageBonus);
      }
    }

    let miscmod = await new Promise((resolve) => {
      new Dialog({
        title: game.i18n.localize('earthdawn.m.miscellaneousModifiers'),
        content: `
            <div class='misc-mod'>
                <label class='misc-mod-description'>${game.i18n.localize('earthdawn.m.modifierStep')}: </label>
                <input id='dialog_box' class='misc-mod-input circleInput' value='${extraSuccess}' autofocus/>
            </div>
            <div class='misc-mod'>
                <label class='misc-mod-description'>${game.i18n.localize('earthdawn.k.karma')}: </label>
                <input id='karma_box' class='misc-mod-input circleInput' value='0' data-type='number' />
            </div>`,
        buttons: {
          ok: {
            label: game.i18n.localize('earthdawn.o.ok'),
            callback: (html) => {
              resolve({
                modifier: html.find('#dialog_box').val(),
                karma: html.find('#karma_box').val(),
              });
            },
          },
        },
        default: 'ok',
      }).render(true);
    });

    let karmaused = 0;
    let karmanew = this.system.karma.value - miscmod.karma;
    let rolltype = inputs.rolltype;
    console.log('rolltype ' + rolltype);
    if (karmanew < 0) {
      ui.notifications.info(game.i18n.localize('earthdawn.k.karmaNo'));
    } else if (miscmod.karma > 0) {
      karmaused = miscmod.karma;
    }
    inputs.finalstep = basestep + Number(miscmod.modifier);
    inputs.finalstep += damageTestModifiers(this, rolltype);
    let karmastring = '';
    if (karmaused > 0) {
      karmastring = `+${karmaused}${this.system.karmaDie}`;
    }
    let diceString = getDice(inputs.finalstep) + karmastring;
    let dice = explodify(diceString);
    let r = new Roll(`${dice}`);
    await r.roll();

    var results = {
      damagetype: inputs.damagetype,
      rolltype: inputs.rolltype,
      talent: inputs.talent,
      finalstep: inputs.finalstep,
      dice: diceString,
      name: this.name,
      result: String(r.result),
      total: r.total,
    };

    inputs.dice = dice;
    inputs.name = this.name;

    chatOutput(results, r);
    if (r.roll && karmaused > 0) {
      karmanew = this.system.karma.value - karmaused;
      this.update({ 'system.karma.value': karmanew });
      inputs.karmaused = karmaused;
    }
    return r.total;
  }

  async applyEffect(spellEffect) {
    this.createEmbeddedDocuments('ActiveEffect', [spellEffect]);
  }

  async parseSpell(spellName, extraSuccess) {
    const spell = this.items.filter(function (item) {
      return item.type === 'spell' && item.name === spellName;
    });
    const spellEffect = spell[0].system.effect.toLowerCase();
    let talent = 'Effect';
    let damagestep;
    let steps = 0;
    let modifier = 0;
    let damagetype;
    let rolltype = 'spellEffect';
    if (spellEffect.includes('wil')) {
      steps = this.system.willpowerStep;
      if (spellEffect.includes('+')) {
        let regex = /([0-9])/g;
        let digitSearch = regex.exec(spellEffect);
        modifier = digitSearch[0];
      }
      if (
        spellEffect.includes('mystic') ||
        spellEffect.includes('mystisch') ||
        spellEffect.includes('physical') ||
        spellEffect.includes('physisch')
      ) {
        rolltype = 'spelldamage';
        talent = 'Damage';
        if (spellEffect.includes('mystic') || spellEffect.includes('mystisch')) {
          damagetype = 'mystic';
        } else if (spellEffect.includes('physical') || spellEffect.includes('physisch')) {
          damagetype = 'physical';
        }
      }
    }
    let willforce = game.i18n.localize('earthdawn.w.willforce');
    let willforceTalent = this.items.getName(willforce);

    damagestep = Number(steps) + Number(modifier);
    if (damagestep === 0) {
      ChatMessage.create({
        content: `<div>${game.i18n.localize('earthdawn.e.effect')}:</div><div>${spell[0].system.effect}</div>`,
        speaker: ChatMessage.getSpeaker({ alias: this.name }),
      });
      return false;
    } else {
      if (this.items.getName(willforce)) {
        let miscmod = await new Promise((resolve) => {
          new Dialog({
            title: `${game.i18n.localize('earthdawn.w.willforce')}?`,
            content: `
                        <div>${game.i18n.localize('earthdawn.w.willforceUse')}?</div>
                            `,
            buttons: {
              ok: {
                label: game.i18n.localize('earthdawn.y.yes'),
                callback: () => {
                  resolve({
                    willforce: true,
                  });
                },
              },
              no: {
                label: game.i18n.localize('earthdawn.n.no'),
                callback: () => {
                  resolve({
                    willforce: false,
                  });
                },
              },
            },
            default: 'ok',
          }).render(true);
        });
        if (miscmod.willforce === true) {
          damagestep += willforceTalent.system.finalranks;
          let newdamage = this.system.damage.value + 1;
          this.update({ 'system.damage.value': newdamage });
        }
      }

      let inputs = {
        damagetype: damagetype,
        rolltype: rolltype,
        damagestep: damagestep,
        extraSuccess: extraSuccess,
        talent: `${talent} (${spellName})`,
      };
      this.effectTest(inputs);
    }
  }

  async rollToInitiative(inputs, r) {
    let array = Array.from(game.combat.combatants);
    let activeCombatant = array.filter((combatant) => combatant.actorId === this.id);
    let combatantID = activeCombatant[0].id;

    await game.combat.setInitiative(combatantID, r.total);
  }

  async NPCtest(inputs) {
    rollPrep(this, inputs);
  }

  async NPCDamage(actor, damage, extraSuccess, powerType) {
    let basestep = damage;
    extraSuccess = extraSuccess * 2;
    let rolltype = '';
    if (powerType === 'Attack') {
      rolltype = 'attackdamage';
    }
    const html = `
    <div class='misc-mod'>
        <label class='misc-mod-description'>${game.i18n.localize('earthdawn.m.modifierStep')}: </label>
        <input id='dialog_box' class='misc-mod-input circleInput' value='${extraSuccess}' autofocus/>
    </div>
    <div class='misc-mod'>
        <label class='misc-mod-description'>${game.i18n.localize('earthdawn.k.karma')}: </label>
        <input id='karma_box' class='misc-mod-input circleInput' value='0' data-type='number' />
    </div>`;
    let miscmod = await new Promise((resolve) => {
      new Dialog({
        title: game.i18n.localize('earthdawn.m.miscellaneousModifiers'),
        content: html,
        buttons: {
          ok: {
            label: game.i18n.localize('earthdawn.o.ok'),
            callback: (html) => {
              resolve({
                modifier: html.find('#dialog_box').val(),
                karma: html.find('#karma_box').val(),
              });
            },
          },
        },
        default: 'ok',
      }).render(true);
    });

    let karmaused = 0;
    let karmanew = this.system.karma.value - miscmod.karma;
    if (karmanew < 0) {
      ui.notifications.info(game.i18n.localize('earthdawn.k.karmaNo'));
    } else {
      karmaused = miscmod.karma;
    }
    let finalstep = Number(basestep) + Number(miscmod.modifier) + damageTestModifiers(this, rolltype);
    let karmaUsedString = karmaused > 0 ? `+ ${karmaused}d6` : '';
    let diceString = getDice(finalstep) + karmaUsedString;
    let dice = explodify(diceString);

    let r = new Roll(`${dice}`);
    await r.roll();

    var results = {
      type: 'effect',
      rolltype: 'weapondamage',
      talent: 'Damage',
      finalstep: finalstep,
      dice: diceString,
      name: this.name,
      result: String(r.result),
      total: r.total,
    };
    chatOutput(results, r);
    if (r.roll && karmaused > 0) {
      karmanew = this.system.karma.value - karmaused;
      this.update({ 'system.karma.value': karmanew });
      inputs.karmaused = karmaused;
    }
  }

  async takeDamage(inputs) {
    let damage = inputs.damage;
    let type = inputs.type;
    let ignorearmor = inputs.ignorearmor;
    let damagetaken;
    let newdamage;

    if (isNaN(damage)) {
      ui.notifications.error(game.i18n.localize('earthdawn.d.damageNoValue'));
      return false;
    } else {
      if (damage > 0 && type === 'physical') {
        if (ignorearmor === true) {
          damagetaken = damage;
        } else {
          damagetaken = damage - this.system.physicalarmor;
        }
      } else if (damage > 0 && type === 'mystic') {
        if (ignorearmor === true) {
          damagetaken = damage;
        } else {
          damagetaken = damage - this.system.mysticarmor;
        }
      } else {
        damagetaken = damage - this.system.physicalarmor;
      }

      if (damagetaken < 0) {
        damagetaken = 0;
      }

      if (isNaN(damagetaken)) {
        ui.notifications.error(game.i18n.localize('earthdawn.d.damageNoValue'));
        return false;
      } else {
        newdamage = Number(this.system.damage.value) + Number(damagetaken);
        await this.update({ 'system.damage.value': newdamage });
        let armormessage =
          ignorearmor === true ? `${game.i18n.localize('earthdawn.a.armorIgnored')}` : `${game.i18n.localize('earthdawn.a.armorApplied')}`;
        let html = `<div>${this.name} ${game.i18n.localize('earthdawn.t.took')} ${damagetaken} ${armormessage}</div>`;
        var newwounds;

        if (this.system.damage.value >= this.system.damage.max) {
          ui.notifications.error(`${this.name} ${game.i18n.localize('earthdawn.i.isDead')}`);
          html += `<div>${this.name} ${game.i18n.localize('earthdawn.i.isDead')}!</div>`;
          if (damagetaken >= this.system.woundThreshold) {
            newwounds = this.system.wounds + 1;
            await this.update({ 'system.wounds': newwounds });
          }
        } else if (this.system.damage.value >= this.system.unconscious.max) {
          ui.notifications.error(`${this.name} ${game.i18n.localize('earthdawn.i.isUnconscious')}`);
          html += `<div>${this.name} ${game.i18n.localize('earthdawn.i.isUnconscious')}`;
          if (damagetaken >= this.system.woundThreshold) {
            newwounds = this.system.wounds + 1;
            await this.update({ 'system.wounds': newwounds, 'system.tactics.knockeddown': true });
          }
        } else {
          if (damagetaken >= this.system.woundThreshold) {
            newwounds = this.system.wounds + 1;
            await this.update({ 'system.wounds': newwounds });
            if (damagetaken >= this.system.woundThreshold + 5) {
              let knockdowndifficulty = damagetaken - this.system.woundThreshold;
              html += `<div>${this.name} ${game.i18n.localize('earthdawn.t.tookWound')}</div><div>${game.i18n.localize(
                'earthdawn.m.makeKnockDown',
              )} ${knockdowndifficulty}</div>`;
              let inputs = { difficulty: knockdowndifficulty };
              this.knockdownTest(inputs);
            }
          }
        }

        ChatMessage.create({
          content: html,
          speaker: ChatMessage.getSpeaker({ alias: this.name }),
        });
      }
    }
  }

  async recoveryTest() {
    let rolltype = 'recovery';
    if (this.system.recoverytestscurrent < 1) {
      rolltype = 'recovery';
      ui.notifications.info(game.i18n.localize('earthdawn.r.recoveryNoLeft'));
      return false;
    } else {
      rolltype = 'recovery';
      let step = this.system.toughnessStep;
      let extraSuccesses = 0;
      let inputs = { damagestep: step, talent: 'Recovery Test', rolltype: 'recovery', extraSuccess: extraSuccesses };
      let result = await this.effectTest(inputs);
      //console.log(this.system.woundsCurrent);
      console.log('INPUTS ' + result);
      result = Number(result) - this.system.woundsCurrent;
      if (result < 1) {
        result = 1;
      }
      //console.log(this.system.damage.value);
      //console.log(result);
      let newdamage = this.system.damage.value - result;
      newdamage = newdamage >= 0 ? newdamage : 0;
      let newrecovery = this.system.recoverytestscurrent - 1;
      this.update({ 'system.damage.value': newdamage, 'system.recoverytestscurrent': newrecovery });
    }
  }

  woundCalc(wounds) {
    let resistPain = game.i18n.localize('earthdawn.r.resistpain');
    let fury = game.i18n.localize('earthdawn.f.fury');
    let finalwounds = wounds;
    if (this.items.filter((item) => item.name.includes(resistPain)).length > 0 && wounds > 0) {
      //ui.notifications.info(game.i18n.localize('earthdawn.r.resistPainNotification'))
      let resistPainName = this.items.filter((item) => item.name.includes(resistPain))[0].name;
      let resistPainValue = Number(resistPainName.replace(/[^0-9]/g, ''));
      finalwounds = resistPainValue >= wounds ? 0 : Number(wounds) - Number(resistPainValue);
    }
    if (this.items.filter((item) => item.name.includes(fury)).length > 0 && wounds > 0) {
      ui.notifications.info(game.i18n.localize('earthdawn.f.furyNotification'));
      let furyName = this.items.filter((item) => item.name.includes(fury))[0].name;
      let furyValue = Number(furyName.replace(/[^0-9]/g, ''));
      if (wounds <= furyValue) {
        finalwounds = -wounds;
      }
    }
    return finalwounds;
  }

  async spellMatrixBox(inputs) {
    const label1 = game.i18n.localize('earthdawn.a.attune');
    const label2 = game.i18n.localize('earthdawn.m.matrixWeaveRed');
    const label3 = game.i18n.localize('earthdawn.m.matrixCastRed');
    const label4 = game.i18n.localize('earthdawn.m.matrixClearRed');
    const label5 = game.i18n.localize('earthdawn.s.spell');
    const label6 = game.i18n.localize('earthdawn.s.spellThreads');
    const matrix = this.items.get(inputs.matrixID);
    const currentSpell = matrix.system.currentspell;
    const currentThreads = matrix.system.activethreads;

    const html = `<p>${label5}: ${currentSpell}</p>
    <p>${label6}: ${currentThreads}</p>
    <select name='spellFunction' id='spellFunction'>
    <option value='cast'>${label3}</option>
    <option value='attune'>${label1}</option>
    <option value='weave'>${label2}</option>
    <option value='clear'>${label4}</option>
    </select>`;

    let option = await new Promise((resolve) => {
      new Dialog({
        title: inputs.matrixName,
        content: html,
        buttons: {
          ok: {
            label: game.i18n.localize('earthdawn.o.ok'),
            callback: (html) => {
              resolve({
                matrixFunction: html.find('#spellFunction').val(),
              });
            },
          },
        },
        default: 'ok',
      }).render(true);
    });

    if (option.matrixFunction === 'attune') {
      this.attuneMatrix(matrix);
    } else if (option.matrixFunction === 'weave') {
      this.weaveThread(matrix);
    } else if (option.matrixFunction === 'cast') {
      this.castSpell(matrix);
    } else if (option.matrixFunction === 'clear') {
      this.clearMatrix(matrix);
    }
  }

  async newDay() {
    if (this.system.damage.value > 0) {
      let updateRslt = await this.update({
        'system.karma.value': this.system.karma.max,
        'system.recoverytestscurrent': this.system.recoverytestsrefreshFinal,
      });
      let testRslt = await this.recoveryTest();
      // announce recovery
      ui.notifications.info(game.i18n.localize('earthdawn.n.newDayRecovery'));
    } else if (this.system.wounds > 0 && this.system.damage.value === 0) {
      let newwounds = this.system.wounds - 1;
      let newtests = this.system.recoverytestsrefreshFinal - 1;
      this.update({
        'system.karma.value': this.system.karma.max,
        'system.recoverytestscurrent': newtests,
        'system.wounds': newwounds,
      });
    } else {
      this.update({
        'system.karma.value': this.system.karma.max,
        'system.recoverytestscurrent': this.system.recoverytestsrefreshFinal,
      });
    }
  }

  /* -------------------------------------------- */
  /*  LP Spending / Upgrading                     */
  /* -------------------------------------------- */

  async upgradeItem(item) {
    if (item.type === 'devotion') {
      ui.notifications.info('Implementation coming soon!');
      return;
    }

    console.debug('[EARTHDAWN] in upgradeItem: BEFORE dialog Promise');
    let options = await new Promise((resolve) => {
      let d = new lpSpendingDialog(this, item, resolve);
      d.render(true);
    });
    console.debug('[EARTHDAWN] in upgradeItem: AFTER dialog Promise');

    if (options.cancel) {
      return false;
    }

    switch (item.type) {
      case 'talent':
        await this._upgradeTalent(item, options);
        break;
      case 'skill':
        await this._upgradeSkill(item, options);
        break;
      case 'discipline':
        await this._upgradeDiscipline(item, options);
        break;
      case 'devotion':
        console.log('Upgrade devotion power: we shouldnt get here :/');
        return;
    }

    options.data.lpTotalAtCreationTime = this.system.legendpointtotal;
    this._writeLPspending(item, options);

    return true;
  }

  async _upgradeTalent(talentItem, options) {
    /*
    Characters can spend Legend Points to add Ranks to existing talents or learn new talents (by purchasing Rank 1)
    The following conditions apply to increasing Talent Ranks

    The character must be rested and in good health
    (not suffering any Current Damage or Wounds, except Blood Magic Damage)

    The character must meditate uninterrupted for eight hours.
    This time is needed for the character to remember his Discipline training,
    imagining how improving his Talent Rank would alter those lessons and exercises.
    Through meditation, he magically extends his initial training to incorporate the new experiences
    and knowledge within himself. The character can only meditate on one talent at a time.

    This cannot be reasonably checked by the system

    The character must not have increased the talent already that day.
    A talent can only be increased by one Rank per day.
    As he also requires sufficient rest, an adept can only increase up to two different talents by one Rank
    each per day under normal circumstances
    (up to two consecutive eight-hour meditation sessions, followed by eight hours of sleep).

    This cannot be reasonably checked by the system

    The character has sufficient Current Legend Points to pay the full cost of the new Talent Rank.
    */

    // process upgrade

    const newCurrentLP = this.system.legendpointcurrent - options.lpCost;
    talentItem.update({ 'system.ranks': options.data.newRank });
    await this.update({ 'system.legendpointcurrent': newCurrentLP });

    const newCurrentSilver = this.system.money.silver - options.silverCost;
    await this.update({ 'system.money.silver': newCurrentSilver });

    ui.notifications.info(
      game.i18n.format('earthdawn.infos.infoItemIncreaseSuccess', {
        itemType: game.i18n.localize('earthdawn.t.talent'),
        itemName: talentItem.name,
        currentRank: options.data.currentRank,
        newRank: options.data.newRank,
        lpCostIncrease: options.lpCost,
      }),
    );
  }

  async _upgradeSkill(skillItem, options) {
    /*
    Improving Skill Ranks

    Characters can spend Legend Points to add Ranks to existing skills, or to learn new skills (by purchasing Rank 1).
    Though similar to gaining Talent Ranks, improving skills differs in the following ways:

    - Characters cannot improve a skill beyond Rank 10.
    - Improving a Skill Rank requires the character to complete a number of weeks of dedicated training equal to the
       new Rank, during which time he must be rested and in good health (not suffering any Current Damage or Wounds,
       except Blood Magic Damage). For example, improving a skill from Rank 3 to Rank 4 requires four weeks of training.
    - The character has sufficient Current Legend Points to pay the full cost of the new Skill Rank (see the
       Skill Training and Cost Table). The cost is determined based on the tier (Novice, Journeyman, etc) listed with
       the skill description.
    - Training a skill costs money. An average week of training costs a character a fee equal to
       the new Rank × 10 silver pieces.
    - Before the character can raise the skill again, he must wait. This time represents the character becoming
       comfortable with the skill at its current level— sometimes referred to as “practice”. During this time the
       character may adventure, raise talent ranks, train for a new Circle, improve other skills, and so forth.
       The time the character must wait is shown on the Skill Training Table. For example, before improving a skill
       from Rank 3 to Rank 4, the character must first wait for five weeks.
    */

    // process upgrade

    const newCurrentLP = this.system.legendpointcurrent - options.lpCost;
    skillItem.update({ 'system.ranks': options.data.newRank });
    await this.update({ 'system.legendpointcurrent': newCurrentLP });

    // TODO: update silver costs

    ui.notifications.info(
      game.i18n.format('earthdawn.infos.infoItemIncreaseSuccess', {
        itemType: game.i18n.localize('earthdawn.s.skill'),
        itemName: skillItem.name,
        currentRank: options.data.currentRank,
        newRank: options.data.newRank,
        lpCostIncrease: options.lpCost,
      }),
    );
  }

  async _upgradeDiscipline(disciplineItem, options) {
    // asdfasdf
  }

  /* -------------------------------------------- */
  /*  Checks                                      */
  /* -------------------------------------------- */

  /**
   * @param {string} type The type of wounds that is to be checked. Possible values: standard, blood, any. Default: any.
   * @returns {boolean} True if there is a positive amount of wounds of the given type marked on this actor, false otherwise.
   */
  hasWounds(type = 'any') {
    const hasStandardWounds = !!parseInt(this.system.wounds);
    const hasBloodWounds = !!parseInt(this.system.bloodMagicWounds);
    const hasAnyWounds = hasStandardWounds || hasBloodWounds;
    return type === 'standard' ? hasStandardWounds : type === 'blood' ? hasBloodWounds : type === 'any' ? hasAnyWounds : undefined;
  }

  /**
   * @param {string} type The type of damage that is to be checked. Possible values: standard, blood, any. Default: any.
   * @returns {boolean} True if there is a positive amount of damage of the given type marked on this actor, false otherwise.
   */
  hasDamage(type = 'any') {
    const hasStandardDamage = !!parseInt(this.system.damage.value);
    const hasBloodDamage = !!parseInt(this.system.bloodMagicDamage);
    const hasAnyDamage = hasStandardDamage || hasBloodDamage;
    return type === 'standard' ? hasStandardDamage : type === 'blood' ? hasBloodDamage : type === 'any' ? hasAnyDamage : undefined;
  }

  getMoneyAs(unit = 'silver') {
    let gold, silver, copper;
    ({ gold, silver, copper } = this.system.money);
    let conversionRate = game.settings.get('earthdawn4e', 'moneyConversionRate');

    switch (unit) {
      case 'gold':
        return gold + silver / conversionRate + copper / conversionRate ** 2;
      case 'silver':
        return gold * conversionRate + silver + copper / conversionRate;
      case 'copper':
        return gold * conversionRate ** 2 + silver * conversionRate + copper;
    }

    /*let g = 0, s = 1, c = 2;

    return (gold * (conversionRate ** g)) +
      (silver * (conversionRate ** s)) +
      (copper * (conversionRate ** c));
    */
  }

  /**
   * Revert records up to index (exclusively)
   */
  async revertLPrecords(index) {
    if (index + 1 >= this.system.lpTracking.currentCount) {
      return;
    }
    const newLPtrackingByInsert = this.system.lpTracking.byInsert.slice(0, index + 1);

    const lpRecordsToDelete = this.system.lpTracking.byInsert.slice(index + 1);
    const lpRecordsToDeleteByItemID = lpRecordsToDelete.reduce((acc, post) => {
      let { itemID } = post;
      return { ...acc, [itemID]: [...(acc[itemID] || []), post] };
    }, {});

    // get LP sum that was spent in the deleted transactions
    const lpToReimburse = lpRecordsToDelete.reduce((prev, curr) => prev + curr.lpCost, 0);
    const newLPcurrent = this.system.legendpointcurrent + lpToReimburse;

    // get the last rank for each item before the deleted transactions
    const earliestRecordPerItemToDelete = Object.values(lpRecordsToDeleteByItemID).map((lpRecArray) =>
      lpRecArray.reduce((prev, curr) => (prev.insertIndex < curr.insertIndex ? prev : curr), Infinity),
    );

    // TODO: do the same stuff for silver?
    await this.updateEmbeddedDocuments(
      'Item',
      earliestRecordPerItemToDelete.map((lpRec) => {
        const { itemID, valueBefore } = lpRec;
        return { _id: itemID, 'system.ranks': valueBefore };
      }),
    );
    await this.update({
      'system.lpTracking.-=byInsert': null,
    });
    await this.update({
      'system.lpTracking.byInsert': newLPtrackingByInsert,
      'system.lpTracking.currentCount': newLPtrackingByInsert.length,
      'system.legendpointcurrent': newLPcurrent,
    });
  }

  async deleteAllLPrecords() {
    await this.update({
      'system.lpTracking.-=byInsert': null,
      'system.lpTracking.currentCount': 0,
    });
  }

  _writeLPspending(item, options) {
    console.debug('[EARTHDAWN] in method _writeLPspending');

    let newByInsert = this.system.lpTracking?.byInsert || [];
    const currentIndex = newByInsert.length;

    let lpRecord = {
      type: options.data.itemType,
      insertIndex: currentIndex,
      creationTime: options.creationTime,
      itemID: item.id,
      itemName: options.data.itemName,
      changeDescription: options.data.changeDescription, // from/to which rank, circle, or just buying of spell
      valueBefore: options.data.valueBefore || -1,
      valueAfter: options.data.valueAfter || -1,
      lpCost: options.lpCost,
      lpBefore: options.data.currentAvailableLP,
      lpTotalAtCreationTime: options.data.lpTotalAtCreationTime,
    };

    newByInsert.push(lpRecord);

    this.update({
      'system.lpTracking.byInsert': newByInsert,
      'system.lpTracking.currentCount': newByInsert.length,
    });
  }
}
