import { ED4E } from './config.js';
import { confirmDeletionDialog } from './dialog-helpers.js';

/**
 * Get the LP cost for learning a given spell
 */
export function getSpellLPcost(spellItem) {
  return spellItem.system?.circle;
}

function getTalentLpCostIncrease(tier, newRank) {
  return ED4E.talentCostTable[tier?.toLowerCase()][newRank];
}

function getSkillLpCostIncrease(tier, newRank) {
  return ED4E.skillCostTable[tier?.toLowerCase()][newRank];
}

function getSkillSilverCostIncrease(trainTimeWeeks, newRank) {
  return newRank * game.settings.get('earthdawn4e', 'avgSilverSkillTrainWeek') * trainTimeWeeks;
}

function getSpendingDataForType(item, actor) {
  let spendingData = {};

  spendingData.itemType = undefined;
  spendingData.itemName = undefined;
  if (typeof item === 'string') {
    spendingData.isAttribute = item.toUpperCase() in ED4E.attributeAbbreviations;
    spendingData.itemType = spendingData.isAttribute ? item.toUpperCase() : item;
    spendingData.itemName = spendingData.isAttribute ? game.i18n.localize(ED4E.attributeAbbreviations[spendingData.itemType]) : item;
  }

  spendingData.itemType = spendingData.itemType ? spendingData.itemType : item.type;
  spendingData.itemName = spendingData.itemName ? spendingData.itemName : item.name;
  spendingData.isDiscipline = spendingData.itemType === 'discipline';
  spendingData.isSkill = spendingData.itemType === 'skill';
  spendingData.isSpell = spendingData.itemType === 'spell';
  spendingData.isTalent = spendingData.itemType === 'talent';
  spendingData.isThread = spendingData.itemType === 'thread';

  spendingData.hasWounds = actor.hasWounds('standard');
  spendingData.hasDamage = actor.hasDamage('standard');

  if (spendingData.isAttribute) {
    return _getSpendingDataForAttribute(item, actor, spendingData);
  } else {
    // assuming item type, maybe add some type checks

    const functionsByType = {
      discipline: _getSpendingDataForDiscipline,
      skill: _getSpendingDataForSkill,
      spell: _getSpendingDataForSpell,
      talent: _getSpendingDataForTalent,
      thread: _getSpendingDataForThread,
    };
    return functionsByType[spendingData.itemType](item, actor, spendingData);
  }
}

function _getSpendingDataForAttribute(item, actor, spendingData = {}) {
  return undefined;
}

function _getSpendingDataForDiscipline(item, actor, spendingData = {}) {
  return undefined;
}

function _getSpendingDataForSkill(item, actor, spendingData = {}) {
  spendingData.currentRank = parseInt(item.system.ranks);
  spendingData.newRank = spendingData.currentRank + 1;
  spendingData.rankTooHigh = spendingData.newRank > 10;
  spendingData.valueBefore = spendingData.currentRank;
  spendingData.valueAfter = spendingData.newRank;

  // The html entity \u21A6 is "rightwards arrow from bar"
  spendingData.changeDescription = `${game.i18n.localize('earthdawn.r.rankIncrease')}: ${spendingData.currentRank} \u21A6 ${
    spendingData.newRank
  }`;

  spendingData.tier = item.system.tier?.toLowerCase() || 'novice';

  spendingData.lpCostIncrease = getSkillLpCostIncrease(item.system.tier, spendingData.newRank);
  spendingData.currentAvailableLP = parseInt(actor.system.legendpointcurrent);

  spendingData.hasSufficientLP = spendingData.lpCostIncrease <= spendingData.currentAvailableLP;

  spendingData.trainingTimeWeeks = spendingData.newRank;
  spendingData.waitingTimeWeeks = ED4E.skillWaitingTime[spendingData.newRank];

  // an average week of training costs a character a fee equal to the new Rank × 10 silver pieces
  spendingData.averageSilverCost = getSkillSilverCostIncrease(spendingData.trainingTimeWeeks, spendingData.newRank);
  spendingData.currentAvailableSilver = actor.getMoneyAs();

  spendingData.hasSufficientSilver = spendingData.averageSilverCost <= actor.getMoneyAs();

  spendingData.anyProblems = !spendingData.hasSufficientLP || spendingData.rankTooHigh || spendingData.hasWounds || spendingData.hasDamage;

  return spendingData;
}

function _getSpendingDataForSpell(item, actor, spendingData = {}) {
  return undefined;
}

function _getSpendingDataForTalent(item, actor, spendingData = {}) {
  spendingData.currentRank = parseInt(item.system.ranks);
  spendingData.newRank = spendingData.currentRank + 1;
  spendingData.rankTooHigh = spendingData.newRank > 15;
  spendingData.valueBefore = spendingData.currentRank;
  spendingData.valueAfter = spendingData.newRank;

  // The html entity \u21A6 is "rightwards arrow from bar"
  spendingData.changeDescription = `${game.i18n.localize('earthdawn.r.rankIncrease')}: ${spendingData.currentRank} \u21A6 ${
    spendingData.newRank
  }`;

  spendingData.tier = item.system.tier?.toLowerCase() || 'novice';
  spendingData.source = item.system.source?.toLowerCase() || 'discipline';
  spendingData.disciplines = actor.items
    .filter((item) => item.type === 'discipline')
    .reduce((acc, post) => {
      return { ...acc, [post.name]: post.id };
    }, {});
  spendingData.discipline = Object.keys(spendingData.disciplines)[0];

  spendingData.lpCostIncrease = getTalentLpCostIncrease(item.system.tier, spendingData.newRank);
  spendingData.currentAvailableLP = parseInt(actor.system.legendpointcurrent);

  spendingData.hasSufficientLP = spendingData.lpCostIncrease <= spendingData.currentAvailableLP;

  spendingData.anyProblems = !spendingData.hasSufficientLP || spendingData.rankTooHigh || spendingData.hasWounds || spendingData.hasDamage;

  return spendingData;
}

function _getSpendingDataForThread(item, actor, spendingData = {}) {
  return undefined;
}

/**
 *
 */
export default class lpSpendingDialog extends Application {
  activateListeners(html) {
    let spendingDataSkill = _getSpendingDataForSkill(this.item, this.actor);
    let spendingDataTalent = _getSpendingDataForTalent(this.item, this.actor);

    // BUTTONS
    html.find("[data-button='cancel']").click((ev) => {
      return this._onCancel();
    });

    html.find("[data-button='ok']").click((ev) => {
      return this._onOK();
    });

    // TALENTS
    html.find('.talent-parameters>#tier').change((ev) => {
      html.find('#lpCost').val(getTalentLpCostIncrease(ev.target.value, spendingDataTalent.newRank)).change();
    });

    // SKILLS
    html.find('.skill-parameters>#tier').change((ev) => {
      html.find('#lpCost').val(getSkillLpCostIncrease(ev.target.value, spendingDataSkill.newRank)).change();
    });

    html.find('.skill-parameters>#trainingTime').change((ev) => {
      html
        .find('#silverCost')
        .val(getSkillSilverCostIncrease(parseInt(ev.target.value), spendingDataSkill.newRank))
        .change();
    });

    html.find('#lpCost').change((ev) => {
      let currentLPcost = parseInt(ev.target.value);
      let availableLP = this.actor.system.legendpointcurrent;
      if (currentLPcost <= parseInt(availableLP)) {
        html.find('#lp-req-label').html(`<i class="fas fa-check-circle"></i> ${game.i18n.localize('earthdawn.s.sufficientLP')}`);
        html.find('#lp-req-text').html(`${game.i18n.localize('earthdawn.o.ok')}`);
      } else {
        html.find('#lp-req-label').html(`<i class="fas fa-exclamation-circle"></i> ${game.i18n.localize('earthdawn.s.sufficientLP')}`);
        html
          .find('#lp-req-text')
          .html(`${game.i18n.format('earthdawn.errors.errorNotEnoughLP', { lpCost: currentLPcost, availableLP: availableLP })}`);
      }
    });

    html.find('#silverCost').change((ev) => {
      let currentSilverCost = parseInt(ev.target.value);
      let availableSilver = this.actor.getMoneyAs();
      if (currentSilverCost <= availableSilver) {
        html.find('#silver-req-label').html(`<i class="fas fa-check-circle"></i> ${game.i18n.localize('earthdawn.s.sufficientSilver')}`);
        html.find('#silver-req-text').html(`${game.i18n.localize('earthdawn.o.ok')}`);
      } else {
        html
          .find('#silver-req-label')
          .html(`<i class="fas fa-exclamation-circle"></i> ${game.i18n.localize('earthdawn.s.sufficientSilver')}`);
        html.find('#silver-req-text').html(
          `${game.i18n.format('earthdawn.errors.errorNotEnoughSilver', {
            silverCost: currentSilverCost,
            availableSilver: availableSilver,
          })}`,
        );
      }
    });

    // DEFAULT KEY BIND
    $(document).on('keydown.chooseDefault', this._onKeyDown.bind(this));
  }

  constructor(actor, item, resolve) {
    super({
      title: game.i18n.localize('earthdawn.s.spendLP'),
    });

    this.resolve = resolve;

    this.actor = actor;
    this.item = item;
    this.isItem = item instanceof Item;
    this.isString = typeof item === 'string';
    this.isAttribute = this.isString ? item.toUpperCase() in ED4E.attributeAbbreviations : false;
    this.attribute = this.isAttribute ? item.toUpperCase() : '';

    if (!(this.isItem || this.isString || this.isAttribute)) {
      throw TypeError("Parameter 'item' must be of type Item or String. If String, allowed values are character attribute abbreviations.");
    }
  }

  /** @override */
  static get defaultOptions() {
    return mergeObject(super.defaultOptions, {
      classes: ['upgradeDialog', 'dialog'],
      template: `systems/earthdawn4e/templates/popups/lp-spending.hbs`,
    });
  }

  get template() {
    return `systems/earthdawn4e/templates/popups/lp-spending.hbs`;
  }

  /** @override */
  getData() {
    let context = super.getData();

    context = {
      ...context,
      ...getSpendingDataForType(this.item, this.actor),
    };

    /*if (!context.newRank) {
      ui.notifications.error(
        game.i18n.format('earthdawn.errors.errorRankOfItemNotParsable', {
          itemType: game.i18n.localize('earthdawn.t.talent'),
          itemName: talentItem.name,
        }),
      );
      this._onCancel();
    }*/

    context.okButtonLabel = game.i18n.localize(context.anyProblems ? 'earthdawn.u.upgradeAnyways' : 'earthdawn.s.spendLP');
    context.localizedOK = game.i18n.localize('earthdawn.o.ok');

    return context;
  }

  submit() {
    this.render(true);
  }

  _onCancel() {
    this.resolve({ cancel: true });
    this.close();
  }

  _getItemType() {
    return this.isItem ? this.item.type : this.isAttribute ? this.attribute : 'unknown type';
  }

  _getItemName() {
    return this.isItem ? this.item.name : this.isAttribute ? ED4E.attributeAbbreviations[this.attribute] : this.item;
  }

  _onOK() {
    // get dynamic data
    //user inputs
    // get <h3> as attribute 'changeDescription'
    this.resolve({
      cancel: false,
      data: this.getData(),
      lpCost: parseInt(this.element.find('#lpCost').val()),
      silverCost: parseInt(this.element.find('#silverCost').val()),
      creationTime: Date.now(),
    });
    this.close();
  }

  async close(options = {}) {
    this.resolve({ cancel: true });
    return super.close(options);
  }

  _onKeyDown(event) {
    // Close dialog
    if (event.key === 'Escape') {
      event.preventDefault();
      event.stopPropagation();
      return this.close();
    }

    // Confirm default choice
    if (event.key === 'Enter') {
      event.preventDefault();
      event.stopPropagation();
      this._onOK();
    }
  }
}

/**
 *
 */
export class lpTrackingTableApp extends Application {
  constructor(actor) {
    super({
      title: game.i18n.localize('earthdawn.l.lpTracking'),
      width: 1000,
      resizable: true,
    });

    this.actor = actor;
  }

  /** @override */
  static get defaultOptions() {
    return mergeObject(super.defaultOptions, {
      classes: ['lpTrackingTable', 'dialog'],
      template: `systems/earthdawn4e/templates/popups/lp-tracking-table.hbs`,
    });
  }

  get template() {
    return `systems/earthdawn4e/templates/popups/lp-tracking-table.hbs`;
  }

  activateListeners(html) {
    html.find('.lpTrackingTableItem').click((ev) => {
      const aTagItemName = ev.currentTarget;
      //aTagItemName.dataset.itemType;
      this.actor.items.get(aTagItemName.dataset.itemId).sheet.render(true);
    });

    html.find('tbody>tr').click((ev) => {
      let tableRow = $(ev.currentTarget);
      console.debug('Clicked Table Row: ', tableRow);
      let revertButton = $('.dialog-button.revert-lp');
      const selectedClass = 'selected';
      if (tableRow.hasClass(selectedClass)) {
        tableRow.removeClass(selectedClass);
        revertButton.prop('disabled', true);
      } else {
        tableRow.addClass(selectedClass).siblings().removeClass(selectedClass);
        revertButton.prop('disabled', false);
      }
    });

    html.find('.revert-lp').click(async (ev) => {
      const reallyDelete = await confirmDeletionDialog();

      if (reallyDelete) {
        const idx = parseInt($('.lpTrackingTable .selected').children().first().text());
        await this.actor.revertLPrecords(idx);
        this.render(true);
      }
    });

    html.find('.delete-all-lp-records').click(async (ev) => {
      const reallyDelete = await confirmDeletionDialog();

      if (reallyDelete) {
        await this.actor.deleteAllLPrecords();
        this.render(true);
      }
    });
  }

  getData(options = {}) {
    let context = super.getData(options);

    context.actor = this.actor;
    context.lpRecords = this.actor.system.lpTracking?.byInsert || [];

    return context;
  }
}
