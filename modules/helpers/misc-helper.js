export function spliceUuid(uuid) {
    const idSplit = uuid.split('.');
    const idObject = {};
    while (idSplit.length > 0) {
        const idKeyValuePair = idSplit.splice(0, 2);
        if (idKeyValuePair.length === 2) {
            idObject[idKeyValuePair.shift().toLowerCase()] = idKeyValuePair.pop();
        }
    }
    return idObject;
}